#!/bin/bash

EXEC_DIR=$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)
LIBS="$EXEC_DIR/lib"

java -Xms64m -Xmx1024m -cp "$LIBS/*.jar" -jar "$LIBS/hierarchy.jar" "$@"

