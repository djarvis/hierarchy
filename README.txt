                ===============================================
                 Hierarchy: Java Class Hierarchy Graphing Tool
                ===============================================

Hierarchy is a Java application for Java software developers. The application
provides a quick way to display the complete superclass and subclass hierarchy
for a given Java class.

ClassAnalyzer is a Java class that exposes an API for custom analysis of
Java classes.

                 ---------------------------------------------
                                   Contents
                 ---------------------------------------------

This release includes:

  - README.txt                 This file
  - LICENSE.txt                License for Hierarchy and ClassAnalyzer

  - build.properties           Version information used by build process
  - build.xml                  Build instructions for Ant
  - ClassAnalyzer.jpr          JDeveloper project file
  - Hierarchy.jws              JDeveloper workspace file
  - run.bat                    DOS batch file to run Hierarhcy
  - run.sh                     UNIX shell script to run Hierarchy

  - lib/bcel-6.0-SNAPSHOT.jar  Bytecode Engineering Library (BCEL)
  - lib/jopt-simple-3.1.0.jar  Command line parsing library
  - lib/prefuse-1.0.0.jar      Graphing library
  - lib/hierarchy.jar          Hierarchy and ClassAnalyzer
  - lib/lucene-1.4.3.jar       Text search engine (used by prefuse)
  - lib/regexp-1.2.0.jar       Regular expression engine (used by prefuse)

  - doc/srs.pdf                Software Requirements Specification
  - doc/technical-overview.pdf Technical Overview of Class Analyzer
  - doc/user-manual.pdf        Explains how to run and use Hierarchy

                 ---------------------------------------------
                                  Requirements
                 ---------------------------------------------

See the User Manual for detailed requirements.

                 ---------------------------------------------
                                  Installation
                 ---------------------------------------------

Installation is complete by unzipping the archive.

