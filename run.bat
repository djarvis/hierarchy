@echo off
SET EXEC_DIR=%~dp0
SET LIBS=%EXEC_DIR%lib

java -Xms64m -Xmx1024m -cp "%LIBS%\*.jar" -jar "%LIBS%\hierarchy.jar" %*

