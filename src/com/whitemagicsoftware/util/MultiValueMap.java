/* Copyright 2009 White Magic Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.whitemagicsoftware.util;

import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

/**
 * <p>
 * Responsible for associating a key with multiple (unique) values, while
 * allowing multiple (unique) keys. This class does not directly store
 * any values. Rather, its internal map stores keys against multiple values.
 * </p>
 *
 * @param <K> The key for mapped values.
 * @param <V> Unique value for the key.
 *
 * @author Dave Jarvis
 */
public class MultiValueMap<K, V>
  extends AbstractMap<K, V> {
  /** Keys and values. Lazily initialized. */
  private Map<K, Collection<V>> map;

  /** Total number of values (counting all keys). */
  private int size;

  /**
   * Creates a default, empty map.
   */
  public MultiValueMap() {
  }

  /**
   * Returns true if the given key can be found somewhere in the map. This
   * does not mean that the key has any associated values.
   *
   * @param key The key to scan for existence.
   * @return true - The key exists in this map.
   */
  public boolean containsKey( Object key ) {
    return getMap().containsKey( key );
  }

  /**
   * Returns true if the given value has been associated with the given key.
   * This method is preferred to containsValue as it is much, much faster.
   * If the key is null, this will return false.
   *
   * @param key The key for which there might be a value.
   * @param value The value which might be associated with the key.
   * @return true - The value is associated with the key.
   */
  public boolean containsKeyValue( K key, V value ) {
    Collection<V> set = getValues( key );

    return set == null ? false : set.contains( value );
  }

  /**
   * Iterates over all keys (which are themselves maps) for the first
   * instance of the given value.
   *
   * @param value The value that might (or might not) be associated with a key.
   * @return true - The value exists in this map.
   */
  public boolean containsValue( Object value ) {
    for( K key : keySet() ) {
      if( getValues( key ).contains( value ) ) {
        return true;
      }
    }

    return false;
  }

  /**
   * Returns the entire set of keys, which themselves include sets of values.
   * This will never return null.
   *
   * @return A valid instance of set.
   */
  public Set<K> keySet() {
    return getMap().keySet();
  }

  /**
   * <p>
   * This method associates a value to a key. Any number of values are allowed,
   * but will only be associated with the key if unique. This will return the
   * child that was associated, if the child was not already associated --
   * otherwise, null.
   * </p>
   *
   * @param key The name of the key for which another value is to be associated.
   * @param value An additional value to associate with the given key.
   * @return Value that was associated, or null if the value already existed.
   * @throws NullPointerException when either key or value are null.
   */
  public V put( K key, V value ) {
    Collection<V> values = getValues( key );
    V result = null;

    // Try to insert the value into the set of values for this key. If the
    // value was not inserted to the set of values (because it already
    // existed), then return null. Otherwise return the value that was inserted
    // into the set. This lets callers know whether the value existed, as per
    // the documented contract.
    //
    if( values.add( value ) ) {
      result = value;
      increaseSize();
    }

    return result;
  }

  /**
   * Removes all the values for the given key, and decreases the size of this
   * map accordingly. This method fails silently.
   *
   * @param key The mapped key for the values to be removed.
   * @return The set of values removed from the map.
   */
  public Collection<V> removeMapping( K key ) {
    Collection<V> values;
    
    synchronized( this ) {
      values = getMap().remove( key );
  
      if( values != null ) {
        decreaseSize( values.size() );
      }
    }

    return values;
  }

  /**
   * Removes the given value from the set of values associated with the given
   * key.
   *
   * @param key The key from which a value is to be removed.
   * @param value The value to remove from the key's set of values.
   * @return true - The key was removed.
   */
  public boolean removeMapping( K key, V value ) {
    boolean removed = false;
    Map<K, Collection<V>> map = getMap();

    synchronized( map ) {
      Collection<V> values = map.get( key );

      if( values != null ) {
        synchronized( this ) {
          removed = values.remove( value );
          decreaseSize( 1 );
        }

        // If there are no more values in the collection, then remove the
        // corresponding key.
        //
        if( values.isEmpty() ) {
          map.remove( key );
        }
      }
    }

    return removed;
  }


  /**
   * Returns the first value associated with the given key.
   *
   * @param key Mapped key whose values are desired.
   * @return The first object mapped to the given key (can be null).
   * @see #getValues( Object )
   */
  public V get( Object key ) {
    Collection<V> values = getMap().get( key );
    Iterator<V> i = values.iterator();

    return i.hasNext() ? i.next() : null;
  }

  /**
   * Returns the list of values for the given key. This will create a new set
   * of values and return it if the key has not been seen before.
   *
   * @param key The key associated with a set of values.
   * @return The values with which the key is associated.
   */
  public Collection<V> getValues( K key ) {
    Collection<V> values = getMap().get( key );

    // If the key does not have a set of values (i.e., the key is brand
    // spanking new), then create a new set (for the values) and associate
    // that set with the given key.
    //
    if( values == null ) {
      values = createSet();
      getMap().put( key, values );
    }

    return values;
  }

  /**
   * Returns the complete mapping of keys to numerous values.
   * This method never returns null.
   *
   * @return The set of entries for this map.
   */
  private Map<K, Collection<V>> getMap() {
    if( this.map == null ) {
      this.map = createMap();
    }

    return this.map;
  }

  /**
   * Returns the set of entries (mapping of the key to multiple values) for
   * this map.
   *
   * @return The set of entries for this map.
   */
  public Set<Map.Entry<K, V>> entrySet() {
    return new EntrySet();
  }

  /**
   * Creates a new set for holding values for a key.
   *
   * @return A new instance of a set, never null.
   */
  protected Collection<V> createSet() {
    return new HashSet<V>( 1024 );
  }

  /**
   * Creates a new map that associates keys with a collection of values.
   *
   * @return A new instance of a map, never null.
   */
  protected Map<K, Collection<V>> createMap() {
    return new HashMap<K, Collection<V>>( 1024 );
  }

  /**
   * Returns how many values have been associated (via the <code>put</code>
   * method).
   *
   * @return The number of mapped values.
   */
  public int size() {
    return this.size;
  }

  /**
   * Increases the count of the number of values in this map.
   */
  private void increaseSize() {
    setSize( size() + 1 );
  }

  /**
   * Decreases the count of the number of values in this map by a given size.
   */
  private void decreaseSize( int by ) {
    setSize( size() - by );
  }

  /**
   * Sets the count of the number of values in this map. The size is
   * never negative.
   *
   * @param size The new number of values.
   */
  private void setSize( int size ) {
    this.size = size;

    if( this.size < 0 ) {
      this.size = 0;
    }
  }

  /**
   * Indicates whether or not the internal map of keys to values contains
   * any keys.
   *
   * @return true - There are no keys for this map.
   */
  public boolean isEmpty() {
    return getMap().isEmpty();
  }

  /**
   * This method does nothing.
   *
   * @param m Not used.
   */
  public void putAll( Map<? extends K, ? extends V> m ) {
  }

  /**
   * Removes all keys from the internal set.
   */
  public void clear() {
    getMap().clear();
  }

  /**
   * Returns the list of all unique values across all keys. This method
   * iterates over all the keys in the internal map, appending the set of
   * values for each key to the set of values to be returned.
   *
   * @return All unique values from all keys.
   */
  public Collection<V> values() {
    Collection<V> result = createSet();

    for( K key : keySet() ) {
      result.addAll( getValues( key ) );
    }

    return result;
  }

  /**
   * Represents a single entry from the MultiValueMap. The EntrySetIterator
   * uses this class to provide clients with a way to iterate over all the
   * values for all the keys in the MultiValueMap.
   */
  private class Entry
    implements Map.Entry<K, V> {
    private K key;
    private V value;

    /**
     * Used by the EntrySetIterator to provide clients with a single key/value
     * pair.
     *
     * @param key The key associated with a number of values.
     * @param value The value associated with the given key.
     */
    public Entry( K key, V value ) {
      this.key = key;
      this.value = value;
    }

    /**
     * Returns the key associated with this entry's value.
     *
     * @return A non-null key.
     */
    public K getKey() {
      return key;
    }

    /**
     * Returns a value associated with this entry's key.
     *
     * @return A non-null value.
     */
    public V getValue() {
      return value;
    }

    /**
     * This method is not supported because there are multiple values
     * associated with a single key.
     *
     * @param value Ignored.
     * @return Does not return gracefully.
     * @throws UnsupportedOperationException Always.
     */
    public V setValue( V value ) {
      throw new UnsupportedOperationException();
    }

    /**
     * The objects are equals if their key and value are equal.
     *
     * @param thatObject The object to compare against this instance.
     * @return true - The given object is equal to this instance.
     */
    public boolean equals( Map.Entry<K, V> thatObject ) {
      try {
        Map.Entry<K, V> that = thatObject;

        return this.getKey().equals( that.getKey() ) &&
          this.getValue().equals( that.getValue() );
      }
      catch( ClassCastException ex ) {
        return false;
      }
    }

    /**
     * Returns the result of the key and value hashcodes XOR'd together.
     *
     * @return An integer value, fairly unique for the key and value.
     */
    public int hashCode() {
      return getKey().hashCode() ^ getValue().hashCode();
    }
  }

  /**
   * Emulates the set of keys and values stored in the MultiValueMap.
   */
  private class EntrySet
    extends AbstractSet<Map.Entry<K, V>> {
    /**
     * Default (empty) constructor.
     */
    protected EntrySet() {
    }

    /**
     * Returns a new EntrySetIterator that can be used to visit each key
     * and each value per key.
     * 
     * @return An iterator for this map's keys and values.
     */
    public Iterator<Map.Entry<K, V>> iterator() {
      return new EntrySetIterator();
    }

    /**
     * Returns the number of values in the map. Each time a key is removed,
     * all its values are removed and this number shrinks accordingly.
     * 
     * @return The number of values in the map.
     */
    public int size() {
      return MultiValueMap.this.size();
    }

    /**
     * Iterates over the map for every key and its values.
     */
    private class EntrySetIterator
      implements Iterator<Map.Entry<K, V>> {
      /** Iterates over all the keys in the MultiValueMap. */
      private Iterator<K> keys = keySet().iterator();

      /** Iterates over the values for each key. */
      private Iterator<V> values;

      /** Most recently returned Key/Value pair returned by this iterator. */
      private Map.Entry<K, V> current;

      /**
       * Default (empty) constructor.
       */
      public EntrySetIterator() {
      }

      /**
       * If the current set of values has more entries, this will return true.
       * If the current set of values does not have more entries, but there
       * are more keys, this will return true. When there are no more keys
       * and no more values for those keys, this will return false.
       *
       * @return true - It is safe to request the next element from the
       * iterator.
       */
      public boolean hasNext() {
        Iterator<V> values = getValues();
        boolean next = values != null && values.hasNext();

        if( !next ) {
          next = getKeys().hasNext();
        }

        return next;
      }

      /**
       * Returns the list of keys associated with at least one value.
       *
       * @return An iterator that can be used to examine all the keys in this
       * map.
       */
      private Iterator<K> getKeys() {
        return this.keys;
      }

      /**
       * Returns the current set of values for the currently iterated key.
       *
       * @return An iterator of values, or null.
       */
      private Iterator<V> getValues() {
        return this.values;
      }

      /**
       * Changes the internal iterator used when scanning the values for a
       * specific key.
       *
       * @param values The list of values associated with a key.
       */
      private void setValues( Iterator<V> values ) {
        this.values = values;
      }

      /**
       * Returns the next key/value pair. Each key/value is visited only once,
       * which means that on every call to next(), the key might be the same
       * as the previous call, but its values will only be visited once each.
       *
       * @return The next value in the map (of keys to values).
       * @throws NoSuchElementException No more keys and no more values.
       */
      public Map.Entry<K, V> next() {
        if( !hasNext() ) {
          throw new NoSuchElementException();
        }

        Map.Entry<K, V> result = null;

        Iterator<V> values = getValues();

        // If there are no more values for this key, get the values for the
        // next key (if a next key exists).
        //
        if( values == null || !values.hasNext() ) {
          final K key = getKeys().next();

          setValues( MultiValueMap.this.getValues( key ).iterator() );

          final V value = getValues().next();

          result = new Entry( key, value );
          setCurrent( result );
        }

        return result;
      }

      /**
       * Removes the current key/value pair from the map.
       */
      public void remove() {
        if( getCurrent() == null ) {
          throw new IllegalStateException();
        }
        MultiValueMap.this
          .remove( getCurrent().getKey(), getCurrent().getValue() );
      }

      /**
       * Changes the current key/value pair for the iterator. This value is
       * also used when calling <code>remove()</code>.
       * 
       * @param current The new map entry value being iterated.
       */
      private void setCurrent( Map.Entry<K, V> current ) {
        this.current = current;
      }

      /**
       * Returns the map entry that is set upon calling <code>next()</code>.
       * 
       * @return The current map entry being iterated.
       */
      private Map.Entry<K, V> getCurrent() {
        return current;
      }
    }
  }
}

