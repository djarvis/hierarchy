/* Copyright 2009 White Magic Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.whitemagicsoftware.util;

/**
 * Responsible for creating DefaultTreeImpl instances. This is the factory
 * used by MultiValueMapTransformer when no other factory is provided. Mostly
 * this is here so that the MultiValueMapTransformer compiles without error
 * (and without forcing clients to make a subclass of MultiValueMapTransformer).
 * Typically, MultiValueMapTransformer is used with a specific factory for
 * creating different types of Tree instances.
 *
 * @param <Payload> The type of data to associate with the tree (node).
 * 
 * @author Dave Jarvis
 *
 * @see com.whitemagicsoftware.util.MultiValueMapTransformer
 */
public class DefaultTreeFactoryImpl<Payload> implements TreeFactory<Payload> {
  /**
   * Default (empty) constructor.
   */
  public DefaultTreeFactoryImpl() {
  }

  /**
   * Returns an instance of DefaultTreeImpl, with the given payload.
   * 
   * @param payload The data to associate with the tree (node).
   * @return A non-null instance of a Tree subclass.
   */
  public Tree create( Payload payload ) {
    return new DefaultTreeImpl<Payload>( payload );
  }
}
