/* Copyright 2009 White Magic Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.whitemagicsoftware.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Given a MultiValueMap, this class is responsible for transforming that map
 * into a tree. The resulting tree will be a directed acyclic graph (DAG),
 * which means that the map must have no circular dependencies.
 *
 * @param <K> The class of keys that are added to the map.
 * 
 * @author Dave Jarvis
 */
public class MultiValueMapTransformer<K> {
  /** The map of keys to values that can be transformed into a DAG. */
  private MultiValueMap<K, K> multiValueMap;

  /** Factory required for creating Tree subclass instances. */
  private TreeFactory<K> treeFactory;
  
  /**
   * Creates a new MultiValueMapTransformer that uses the DefaulTreeFactoryImpl
   * to create hierarchical trees.
   * 
   * @param mvm The map of values to transform into a tree.
   */
  public MultiValueMapTransformer( MultiValueMap<K, K> mvm ) {
    this( mvm, new DefaultTreeFactoryImpl<K>() );
  }

  /**
   * @param mvm The map of values to transform into a tree.
   * @param treeFactory Used to instantiate a specific Tree implementation.
   */
  public MultiValueMapTransformer( MultiValueMap<K, K> mvm,
                                   TreeFactory<K> treeFactory ) {
    setMultiValueMap( mvm );
    setTreeFactory( treeFactory );
  }

  /**
   * Transforms the map into a tree. Algorithm by Stephen.
   * http://stackoverflow.com/questions/1250169/transform-map-with-multiple-values-to-tree
   *
   * @return The set of trees that represent the list of keys and values stored
   * in a MultiValueMap.
   */
  public Set<Tree> transform() {
    MultiValueMap<K, K> input = getMultiValueMap();

    Set<K> keys = input.keySet();
    Set<K> roots = new HashSet<K>( keys );
    Map<K, Tree> map = new HashMap<K, Tree>();

    for( K key : keys ) {
      Tree tree = map.get( key );

      if( tree == null ) {
        tree = createTree( key );
        map.put( key, tree );
      }

      for( K childKey : input.getValues( key ) ) {
        roots.remove( childKey );

        Tree child = map.get( childKey );

        if( child == null ) {
          child = createTree( childKey );
          map.put( childKey, child );
        }

        tree.add( child );
      }
    }

    Set<Tree> result = new HashSet<Tree>( roots.size() );

    for( K key : roots ) {
      result.add( map.get( key ) );
    }

    return result;
  }

  /**
   * Changes the map that is to be transformed into a set of trees.
   * 
   * @param multiValueMap The map on the brink of transformation.
   */
  public void setMultiValueMap( MultiValueMap<K, K> multiValueMap ) {
    this.multiValueMap = multiValueMap;
  }

  /**
   * Returns the map that can be transformed into a set of trees.
   * 
   * @return The map on the brink of transformation.
   */
  private MultiValueMap<K, K> getMultiValueMap() {
    return multiValueMap;
  }

  /**
   * Creates a Tree instance with associated data. This uses a factory method
   * to create the desired type of Tree subclass.
   * 
   * @param payload The data to associate with the node.
   * @return A non-null tree (node).
   * @see com.whitemagicsoftware.util.TreeFactory
   */
  protected Tree createTree( K payload ) {
    return getTreeFactory().create( payload );
  }

  /**
   * Sets the tree factory used for instantiating Tree instances. The
   * treeFactory parameter must not be null.
   * 
   * @param treeFactory The new Tree instantiator.
   */
  private void setTreeFactory( TreeFactory<K> treeFactory ) {
    this.treeFactory = treeFactory;
  }

  /**
   * Returns the factory used to create Tree instances.
   * 
   * @return A non-null TreeFactory.
   */
  private TreeFactory<K> getTreeFactory() {
    return treeFactory;
  }
}

