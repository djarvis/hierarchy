/* Copyright 2009 White Magic Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.whitemagicsoftware.util;

/**
 * Used by the MultiValueMapTransformer to instantiate Tree objects.
 * 
 * @param <Payload> The data carried by a tree (node).
 * 
 * @author Dave Jarvis
 */
public interface TreeFactory<Payload> {
  /**
   * Creates a specific type of tree subclass. This removes knowledge from
   * the MultiValueMapTransformer on what type of tree it needs to create.
   * 
   * @param payload The data to give to the Tree upon instantiation.
   * @return A non-null Tree subclass.
   * @see com.whitemagicsoftware.util.MultiValueMapTransformer
   */
  public Tree create( Payload payload );
}

