/* Copyright 2009 White Magic Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.whitemagicsoftware.util;

import java.util.Set;

/**
 * Represents a directed acyclic graph tree, with each child linking to its
 * parent. The accessibility of the parent remains up to implementors.
 * 
 * @author Dave Jarvis
 */
public interface Tree {
  /**
   * Sets the tree that encloses this tree.
   *
   * @param parent The enclosing tree.
   */
  public void setParent( Tree parent );

  /**
   * Returns the set of trees that are the leaves of this branch. This must
   * never return null, but is allowed to return an empty set of children.
   * 
   * @return Leaves for this branch, or an empty set.
   */
  public Set<Tree> getChildren();

  /**
   * Appends the child tree to this tree's list of children. The child
   * tree must not be null.
   * 
   * @param child The tree to append to this tree's branches.
   */
  public void add( Tree child );
  
  /**
   * Recursively visits every leaf for every branch in the tree.
   */
  public void walk();
}

