/* Copyright 2009 White Magic Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.whitemagicsoftware.util;

import java.util.HashSet;
import java.util.Set;

/**
 * Used to create a tree capable of having an arbitrary data payload. Each
 * Tree instance can be considered a "node" within the tree, which contains
 * the data specified by the payload.
 *
 * @param <Payload> The class of data associated with each node.
 * 
 * @author Dave Jarvis
 */
public class DefaultTreeImpl<Payload>
  implements Tree {

  /** The containing branch of this tree. */
  private Tree parent;

  /** The branches and leaves for this tree. */
  private Set<Tree> children;

  /** Arbitrary data associated with this node. */
  private Payload payload;

  /**
   * Initializes the payload with the given data. The node should actually
   * contain data. The behaviour for passing in null payloads is untested,
   * thus undefined. All hope abandon ye who enter null here.
   * 
   * @param payload The data to associate with this node. Do not use null.
   */
  public DefaultTreeImpl( Payload payload ) {
    setPayload( payload );
  }

  /**
   * Returns the parent tree for this tree, or null if there has been no
   * parent assigned.
   *
   * @return The parent tree, or null.
   */
  protected Tree getParent() {
    return this.parent;
  }

  /**
   * Changes the parent tree for this tree. The given parent can be null.
   *
   * @param parent The new tree parenting this tree.
   */
  public void setParent( Tree parent ) {
    this.parent = parent;
  }

  /**
   * Adds the given tree as a branch of this tree.
   *
   * @param child The tree to add to this tree's list of children.
   */
  public void add( Tree child ) {
    getChildren().add( child );
    child.setParent( this );
  }

  /**
   * Returns the set of subtrees for this tree.
   *
   * @return A set of trees, which might be empty, but will not be null.
   */
  public Set<Tree> getChildren() {
    if( this.children == null ) {
      this.children = createChildren();
    }

    return this.children;
  }

  /**
   * Creates an empty set of subtrees for this tree.
   *
   * @return An empty set, never null.
   */
  protected Set<Tree> createChildren() {
    return new HashSet<Tree>();
  }

  /**
   * Sets the object data which is part of this (tree) node. This will not
   * throw a NullPointerException if the payload is null, but the behaviour
   * has not been rigorously tested.
   *
   * @param payload The data to associate with the node.
   */
  private void setPayload( Payload payload ) {
    this.payload = payload;
  }

  /**
   * Returns the data associated with this node.
   * 
   * @return Data given at time of object construction.
   */
  public Payload getPayload() {
    return payload;
  }
  
  /**
   * Subclasses implement this method to do perform useful work. This is
   * implemented with a no-op because without it it would force clients to
   * develop subclasses before being able to do anything useful with the
   * API.
   */
  public void walk() {
    // Let your subclasses do the walking.
    //
  }
}

