/* Copyright 2009 White Magic Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.whitemagicsoftware.hierarchy.analyzers;


import com.whitemagicsoftware.hierarchy.ClassAnalyzer;
import com.whitemagicsoftware.hierarchy.ClassPathUpdater;
import com.whitemagicsoftware.hierarchy.exceptions.ClasspathException;

import java.io.File;
import java.io.IOException;

import static java.util.Arrays.asList;
import java.util.Collection;

import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;


/**
 * Superclass of all ClassAnalyzers in this package. This class abstracts
 * parsing command line options for ClassAnalyzers in this package.
 *
 * @author Dave Jarvis
 */
public abstract class BaseClassAnalyzer
  extends ClassAnalyzer {
  /** Path separator character = {:, ;} for {Unix, Windows}. */
  protected static final char PS = File.pathSeparatorChar;

  private OptionParser optionParser;

  /**
   * Default (empty) constructor.
   */
  public BaseClassAnalyzer() {
  }

  /**
   * Adds to the classpath using a set of properties defined in the given
   * collection.
   *
   * @param properties The list of properties to use as sources for additional
   * classpath elements.
   * @throws ClasspathException The classpath could not be updated.
   */
  protected void initClasspath( Collection<String> properties )
    throws ClasspathException {
    for( String property : properties ) {
      ClassPathUpdater.addPaths( System.getProperty( property ) );
    }
  }

  /**
   * Adds the sun.boot.class.path directories and archives to the classpath.
   *
   * @throws ClasspathException Could not add the boostrapped classes to the
   * classpath.
   */
  protected void initBootstrapClasspath()
    throws ClasspathException {
    ClassPathUpdater.addPaths( System.getProperty( "sun.boot.class.path" ) );
  }

  /**
   * Parses the command line options provided to the application when it first
   * starts.
   *
   * @param args The command line arguments.
   * @return The OptionSet resulting from parsing the command line options.
   * @throws IOException Could not write to standard output.
   * @throws ClasspathException Could not set the classpath.
   */
  protected OptionSet parseOptions( String[] args )
    throws IOException,
           ClasspathException {
    OptionParser parser = getOptionParser();

    OptionSpec<File> classes =
      parser.acceptsAll( asList( "c", "classes" ), "Class name to analyze." )
      .withRequiredArg().describedAs( "C1.class" + PS + "C2.class" )
      .ofType( File.class ).withValuesSeparatedBy( PS );
    OptionSpec<File> classpath =
      parser.acceptsAll( asList( "cp", "classpath" ),
                         "Directory or Java archive to search." )
      .withRequiredArg().describedAs( "dir" + PS + "file.jar" )
      .ofType( File.class ).withValuesSeparatedBy( PS );
    OptionSpec<String> properties =
      parser.acceptsAll( asList( "p", "properties" ),
                         "Search System property classpaths." )
      .withRequiredArg()
      .describedAs( "java.ext.dirs" + PS + "java.library.path" )
      .ofType( String.class ).withValuesSeparatedBy( PS );

    OptionSet options = parser.parse( args );

    if( options.has( "help" ) ) {
      showHelp();
    }
    
    if( options.has( "classpath" ) ) {
      updateClassPath( options.valuesOf( classpath ) );
    }

    if( options.has( "properties" ) ) {
      initClasspath( options.valuesOf( properties ) );
    }

    if( options.has( "classes" ) ) {
      addClassFiles( options.valuesOf( classes ) );
    }

    if( options.has( "exclude" ) ) {
      excludeSimple( ( String )options.valueOf( "exclude" ) );
    }

    if( options.has( "display-classpath" ) ) {
      displayClasspath();
    }

    initBootstrapClasspath();

    return options;
  }

  /**
   * Lazily creates a new instance of the command line parser. This will
   * always return a new, valid instance of OptionParser.
   *
   * @return The command line parser, never null.
   */
  protected OptionParser getOptionParser() {
    if( this.optionParser == null ) {
      this.optionParser = createOptionParser();
    }

    return this.optionParser;
  }

  /**
   * Creates a new instance of the command line parser. This will
   * always return a new, valid instance of OptionParser. The parser is
   * initialized with help, exclusion, and display classpath parameters.
   *
   * @return The command line parser, never null.
   */
  protected OptionParser createOptionParser() {
    return new OptionParser() {
      {
        acceptsAll( asList( "h", "?", "help" ), "Show this help text." );
        acceptsAll( asList( "e", "exclude" ),
                    "Ignore packages and classes." ).withRequiredArg()
          .describedAs( "java.awt.*|java.nio.*" );
        acceptsAll( asList( "dc", "display-classpath" ),
                    "Write classpath to standard out." );
      }
    };
  }

  /**
   * Writes the help to standard out and then terminates with error level 1.
   *
   * @throws IOException Could not write to standard output.
   */
  protected void showHelp()
    throws IOException {
    getOptionParser().printHelpOn( System.out );
    exit();
  }
  
  /**
   * Terminates the application with error level 1.
   */
  protected void exit() {
    System.exit( 1 );
  }

  private void displayClasspath() {
    System.out.println( "CLASSPATH=" + ClassPathUpdater.getSystemClassPath() );
  }
}
