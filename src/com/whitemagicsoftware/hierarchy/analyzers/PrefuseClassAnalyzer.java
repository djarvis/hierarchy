/* Copyright 2009 White Magic Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.whitemagicsoftware.hierarchy.analyzers;


import com.whitemagicsoftware.hierarchy.exceptions.ClasspathException;
import com.whitemagicsoftware.hierarchy.exceptions.HierarchyNotFoundException;
import com.whitemagicsoftware.util.DefaultTreeImpl;
import com.whitemagicsoftware.util.MultiValueMap;
import com.whitemagicsoftware.util.MultiValueMapTransformer;
import com.whitemagicsoftware.util.Tree;
import com.whitemagicsoftware.util.TreeFactory;

import java.io.IOException;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import org.apache.bcel.classfile.JavaClass;


/**
 * Example that creates a
 * <a href="http://www.prefuse.org/">prefuse</a>-compatible XML document.
 *
 * @author Dave Jarvis
 */
public class PrefuseClassAnalyzer
  extends BaseClassAnalyzer {
  /** Maps superclasses to lists of subclasses. */
  private MultiValueMap<JavaClass, JavaClass> map =
    new MultiValueMap<JavaClass, JavaClass>();

  /**
   * Default (empty) constructor.
   */
  public PrefuseClassAnalyzer() {
  }

  /**
   * Automatically called by <code>run()</code>. The <code>run()</code> method
   * handles reverting the classpath.
   */
  public void doAnalysis() {
    try {
      // Begin calling the visit methods below.
      //
      analyze();
    }
    catch( ClassNotFoundException ex ) {
      System.err.println( ex.toString() );
    }
    catch( ClasspathException ex ) {
      System.err.println( ex.toString() );
    }
    catch( HierarchyNotFoundException ex ) {
      System.err.println( ex.toString() );
    }
  }

  /**
   * See superclass comment for details.
   *
   * @param parent The superclass of child.
   * @param child A subclass of parent.
   */
  public void visitSubclass( JavaClass parent, JavaClass child ) {
    put( parent, child );
  }

  /**
   * See superclass comment for details.
   *
   * @param parent The superclass of child.
   * @param child A subclass of parent.
   */
  public void visitSuperclass( JavaClass parent, JavaClass child ) {
    put( parent, child );
  }

  /**
   * Helper method to bring together a parent with its child.
   *
   * @param parent The superclass of child.
   * @param child A subclass of parent.
   */
  private void put( JavaClass parent, JavaClass child ) {
    getMap().put( parent, child );
  }

  /**
   * The map used to track parent-child relationships.
   *
   * @return A non-null map.
   */
  private MultiValueMap<JavaClass, JavaClass> getMap() {
    return this.map;
  }

  /**
   * Transforms the mapped data (i.e., the complete set of subclass hierarchies)
   * into a set of trees.
   *
   * @return The trees that were converted from the mapped data returned
   * from getMap().
   */
  protected Set<Tree> transform() {
    return createTransformer().transform();
  }

  /**
   * Creates a transformer engine capable of converting a map (consisting of
   * directed acyclic graph data) into a bonifide hierarchical tree structure.
   *
   * @return A non-null transformer, ready to transform the class relationship
   * map into a tree.
   */
  private MultiValueMapTransformer<JavaClass> createTransformer() {
    return new MultiValueMapTransformer<JavaClass>( getMap(),
                                                    getTreeFactory() );
  }

  /**
   * Returns the factory that is used to tell the MultiValueMapTransformer what
   * type of trees to instantiate.
   *
   * @return A non-null TreeFactory subclass.
   */
  protected TreeFactory<JavaClass> getTreeFactory() {
    return new PrefuseTreeFactoryImpl<JavaClass>();
  }

  /**
   * Responsible for writing an XML document to standard output that is
   * compatiable with prefuse.
   *
   * @param <Payload> The type of data contained by each node in the tree.
   */
  protected class PrefuseTreeImpl<Payload>
    extends DefaultTreeImpl<Payload>
    implements Comparable<Payload> {
    /**
     * Used to create a hierarchical tree where every tree is a node with
     * data.
     *
     * @param payload The data for this tree (node).
     */
    public PrefuseTreeImpl( Payload payload ) {
      super( payload );
    }

    /**
     * Writes a complete, hierarchical XML document to standard output. The
     * document is in a format that can be displayed by prefuse.
     */
    public void walk() {
      prefuseOutputBegin();
      walk( this, 0 );
      prefuseOutputEnd();
    }

    /**
     * Writes the XML header and XML declarations (required by prefuse) to
     * standard output.
     */
    protected void prefuseOutputBegin() {
      System.out.printf( "<?xml version='1.0'?>\n" );
      System.out.printf( "<tree>\n" );
      System.out.printf( "  <declarations>\n" );
      System.out.printf( "    <attributeDecl name='name' type='String' />\n" );
      System.out.printf( "  </declarations>\n" );
    }

    /**
     * Writes the XML footer to standard output.
     */
    protected void prefuseOutputEnd() {
      System.out.printf( "</tree>\n" );
    }

    /**
     * Writes each leaf and branch of the tree to standard output. The
     * level is used to determine indenting. While technically superfluous, it
     * makes it easier for humans to read (and possibly debug).
     *
     * @param tree The tree containing branches and leaves to write to
     * standard output.
     * @param level The depth of the tree at this point.
     */
    protected void walk( Tree tree, int level ) {
      Set<Tree> children = new TreeSet<Tree>( tree.getChildren() );

      String spaces = getIndent( ++level );

      PrefuseTreeImpl treeImpl = (PrefuseTreeImpl)tree;

      if( children.isEmpty() ) {
        System.out.printf( "%s<leaf>\n", spaces );
        System.out.printf( "%s  <attribute name='name' value='%s' />\n",
                           spaces,
                           ((JavaClass)(treeImpl.getPayload())).getClassName());
        System.out.printf( "%s</leaf>\n", spaces );
      }
      else {
        System.out.printf( "%s<branch>\n", spaces );
        System.out.printf( "%s  <attribute name='name' value='%s' />\n",
                           spaces,
                           ((JavaClass)(treeImpl.getPayload())).getClassName());

        for( Tree child : children ) {
          walk( child, level );
        }

        System.out.printf( "%s</branch>\n", spaces );
      }
    }

    /**
     * Indentation for XML readability.
     *
     * @param level Indent depth.
     * @return A string with spaces.
     */
    private String getIndent( int level ) {
      // Optimal creation for less than 64 nested levels.
      //
      StringBuilder spaces = new StringBuilder( 128 );

      for( int i = 0; i < level; i++ ) {
        spaces.append( "  " );
      }

      return spaces.toString();
    }

    /**
     * Indicates whether the given object stands in relation to this object,
     * alphabetically. Negative integers indicate that the given object
     * comes earlier in the alphabet, positive integers indicate it comes
     * later.
     *
     * @param o An instance of PrefuseTreeImpl.
     * @return -1 if the given object is alphabetically sooner, 0 if they
     * are the same, and +1 if the given object comes later.
     * @throws ClassCastException o instanceof PrefuseTreeImpl == false
     */
    public int compareTo( Object o ) {
      if( o instanceof PrefuseTreeImpl ) {
        PrefuseTreeImpl that = ( PrefuseTreeImpl )o;
        JavaClass thatPayload = ( JavaClass )( that.getPayload() );
        JavaClass thisPayload = ( JavaClass )( getPayload() );

        return thisPayload.getClassName()
          .compareTo( thatPayload.getClassName() );
      }

      // Uncomparable.
      //
      throw new ClassCastException( o.getClass().toString() );
    }
  }

  /**
   * Factory used by <code>getTreeFactory()</code> to create trees suitable
   * for the PrefuseClassAnalyzer.
   *
   * @param <Payload> Data type contained by each tree (node).
   */
  protected class PrefuseTreeFactoryImpl<Payload>
    implements TreeFactory<Payload> {

    /**
     * Default (empty) constructor.
     */
    public PrefuseTreeFactoryImpl() {
    }

    /**
     * Creates an instance of Tree with data for the node.
     *
     * @param payload Data for the node.
     * @return A non-null instance of a Tree subclass.
     */
    public Tree create( Payload payload ) {
      return new PrefuseTreeImpl<Payload>( payload );
    }
  }

  /**
   * Program entry point. Primarily used for testing the ClassAnalyzer.
   *
   * @param args Parameters to use to control the ClassAnalyzer.
   * @throws InterruptedException Could not wait for the thread to run.
   * @throws IOException Could not write to System.out.
   * @throws ClasspathException Could not set classpath.
   */
  public static void main( String[] args )
    throws InterruptedException,
           IOException,
           ClasspathException {
    PrefuseClassAnalyzer pca = new PrefuseClassAnalyzer();
    Thread thread = new Thread( pca );

    pca.parseOptions( args );
    thread.run();
    thread.join();

    Set<Tree> treeSet = pca.transform();
    Iterator<Tree> iterator = treeSet.iterator();

    iterator.next().walk();
  }
}
