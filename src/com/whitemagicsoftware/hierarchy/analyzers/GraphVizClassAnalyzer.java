/* Copyright 2009 White Magic Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.whitemagicsoftware.hierarchy.analyzers;


import com.whitemagicsoftware.hierarchy.exceptions.ClasspathException;

import com.whitemagicsoftware.hierarchy.exceptions.HierarchyNotFoundException;

import java.io.IOException;

import org.apache.bcel.classfile.JavaClass;


 /**
  * Example that creates a
  * <a href="http://www.graphviz.org/">GraphViz</a>-compatible dot graph.
  * 
  * @author Dave Jarvis
  */
public class GraphVizClassAnalyzer
  extends BaseClassAnalyzer {

  /**
   * Default (empty) constructor.
   */
  public GraphVizClassAnalyzer() {
  }

  /**
   * This method is called once the ClassAnalyzer has set the classpath and
   * is ready to perform analysis on all the class files therein.
   * 
   * @see #run()
   */
  public void doAnalysis() {
    dotOutputBegin();

    try {
      // Begin calling the visit methods.
      //
      analyze();
    }
    catch( ClassNotFoundException ex ) {
      System.err.println( ex.toString() );
    }
    catch( ClasspathException ex ) {
      System.err.println( ex.toString() );
    }
    catch( HierarchyNotFoundException ex ) {
      System.err.println( ex.toString() );
    }

    dotOutputEnd();
  }

  /**
   * @param parent The superclass of child.
   * @param child A subclass of parent.
   */
  public void visitSubclass( JavaClass parent, JavaClass child ) {
    dotOutput( parent, child );
  }

  /**
   * @param parent The superclass of child.
   * @param child A subclass of parent.
   */
  public void visitSuperclass( JavaClass parent, JavaClass child ) {
    dotOutput( parent, child );
  }

  /**
   * Writes the header for dot files to standard output. This must take place
   * before the analyis is performed.
   */
  private void dotOutputBegin() {
    System.out.println( "digraph G {" );
    System.out.println( "node [shape=box];" );
    System.out.println( "rankdir=LR;" );
  }

  /**
   * Writes the footer for dot files to standard output. This must take place
   * after the analyis is performed.
   */
  private void dotOutputEnd() {
    System.out.println( "}" );
  }

  /**
   * Helper method called by visitSubclass and visitSuperclass to write their
   * relationship to standard output (in dot format, of course).
   * 
   * @param parent The immediate superclass of the child.
   * @param child An immediate subclass of the parent.
   */
  private void dotOutput( JavaClass parent, JavaClass child ) {
    System.out.print( "\"" );
    System.out.print( parent.getClassName() );
    System.out.print( "\" -> \"" );
    System.out.print( child.getClassName() );
    System.out.println( "\"" );
  }
  
  /**
   * Program entry point. Primarily used for testing the ClassAnalyzer.
   *
   * @param args Parameters to use to control the ClassAnalyzer.
   * @throws InterruptedException Could not wait for the thread to run.
   * @throws IOException Could not write to System.out.
   * @throws ClasspathException Could not set classpath.
   */
  public static void main( String[] args )
    throws InterruptedException,
           IOException,
           ClasspathException {
    GraphVizClassAnalyzer ca = new GraphVizClassAnalyzer();
    Thread thread = new Thread( ca );

    ca.parseOptions( args );
    thread.run();
    thread.join();
  }
}
