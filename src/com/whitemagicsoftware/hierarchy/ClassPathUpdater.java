/* Copyright 2009 White Magic Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.whitemagicsoftware.hierarchy;

import com.whitemagicsoftware.hierarchy.exceptions.ClasspathException;

import java.io.File;
import java.io.FileNotFoundException;

import java.lang.reflect.Method;

import java.net.URL;
import java.net.URLClassLoader;

/**
 * Responsible for runtime classpath modifications.
 * 
 * @author Dave Jarvis
 */
public class ClassPathUpdater {
  /** Used to find the method signature. */
  private static final Class<?>[] PARAMETERS = new Class<?>[] { URL.class };

  /** Class containing the private addURL method. */
  private static final Class<?> CLASS_LOADER = URLClassLoader.class;

  /** Method to call on the current class loader. */
  private static final String ADD_URL = "addURL";

  /** Filename separator character (for classpath changes): : or ; */
  private static final String NS = File.pathSeparator;

  /** System.getProperty property name representing the main classpath. */
  private static final String PROPERTY_CLASSPATH = "java.class.path";

  /** Initialized when the ClassPathUpdater is loaded. */
  private static Method addURL;

  /**
   * Utility class does not need to be instantiated.
   */
  private ClassPathUpdater() {
  }

  static {
    // There is no point trying to execute this each time one of the add
    // methods are called. If this doesn't work when the class is loaded then
    // it won't work when the class is running! The application should be
    // written such that it can continue without depending on runtime changes
    // to the classpath.
    //
    try {
      addURL = CLASS_LOADER.getDeclaredMethod( ADD_URL, PARAMETERS );
      addURL.setAccessible( true );
    }
    catch( NoSuchMethodException ex ) {
      System.err.println( ex );
    }
  }
  
  /**
   * Adds a set of paths (using the standard separator character) to the
   * paths that are searched.
   *
   * @param paths The paths to search when looking for classes.
   * @throws ClasspathException One of the paths was null.
   */
  public static void addPaths( String paths )
    throws ClasspathException {
    for( String path : paths.split( NS ) ) {
      try {
        add( path );
      }
      catch( ClasspathException ex ) {
        if( !(ex.getCause() instanceof FileNotFoundException) ) {
          throw ex;
        }
      }
    }
  }

  /**
   * Adds the given string, which can represent a file, directory, or Java
   * archive to the classpath.
   *
   * @param s The directory to add to the classpath (or a file, which
   * will relegate to its directory).
   * @throws ClasspathException Classpath could not be set (cause has details).
   * @see #add( File f )
   */
  public static void add( String s )
    throws ClasspathException {
    add( new File( s ) );
  }

  /**
   * Adds a new path to the classloader and the classpath. If the file name
   * ends in ".class", then its containing directory is added to the classpath.
   * Otherwise it is presumed to be a Java archive of some sort (jar, ear, war,
   * zip, etc.) or a directory, and is added to the classpath verbatim.
   *
   * @param f The directory, archive, or class to add to the classpath.
   * @throws ClasspathException Classpath could not be set (cause has details).
   */
  public static void add( File f )
    throws ClasspathException {
    if( f == null ) {
      classpathException( new IllegalArgumentException( "Path is null." ) );
    }
    else if( !f.exists() ) {
      classpathException( new FileNotFoundException( f.getAbsolutePath() ) );
    }

    String pathName = f.getAbsolutePath();

    if( pathName.endsWith( ".class" ) ) {
      f = f.getParentFile();
      pathName = f.getAbsolutePath();
    }

    String classPath = getSystemClassPath();

    // Do not append to the classpath if the path was already added.
    //
    if( classPath.lastIndexOf( pathName ) == -1 ) {
      // Ensure no further attempts to add the path to the classpath by
      // setting the System's classpath property.
      //
      setSystemClassPath( classPath + NS + pathName );

      // Finally, add the fully qualified path to the class loader.
      //
      try {
        add( f.toURI().toURL() );
      }
      catch( Exception ex ) {
        classpathException( ex );
      }
    }
  }

  /**
   * Adds the given URL to the classpath.
   *
   * @param url The path to include when searching for classes.
   * @throws ClasspathException Classpath could not be set (cause has details).
   * @see #add( File f )
   */
  private static void add( URL url )
    throws ClasspathException {
    try {
      getAddURL().invoke( getCurrentClassLoader(), new Object[] { url } );
    }
    catch( Exception ex ) {
      classpathException( ex );
    }
  }

  /**
   * Returns the list of directories and Java archives from the classpath.
   *
   * @return An array of strings, each string representing a separate part
   * of the classpath.
   */
  public static String[] getClassPathElements() {
    return getSystemClassPath().split( NS );
  }

  /**
   * Returns the method used to add a path to the classpath (at runtime).
   *
   * @return A reflected method, possibly null.
   */
  private static Method getAddURL() {
    return addURL;
  }

  /**
   * Returns the class loader associated with the current thread. This could
   * return null if no class loader has been associated with the thread
   * (unlikely as that may be).
   *
   * @return An instance of ClassLoader, possibly null.
   */
  private static ClassLoader getCurrentClassLoader() {
    return Thread.currentThread().getContextClassLoader();
  }

  /**
   * Wraps any exception that causes the classpath update to fail (as its
   * cause), then throws a ClasspathException to indicate the failure.
   *
   * @param cause What triggered updating the classpath to fail.
   * @throws ClasspathException Always thrown.
   */
  private static void classpathException( Exception cause )
    throws ClasspathException {
    throw new ClasspathException( cause );
  }

  /**
   * Returns the value of the System property "java.class.path". If the
   * path has not been set, this will return the empty string.
   *
   * @return A non-null String instance.
   */
  public static String getSystemClassPath() {
    String path = System.getProperty( PROPERTY_CLASSPATH );
    return path == null ? "" : path;
  }

  /**
   * Changes the value of "java.class.path" to the given path.
   *
   * @param path The new value for the java.class.path System property.
   */
  public static void setSystemClassPath( String path ) {
    System.setProperty( PROPERTY_CLASSPATH, path );
  }
}

