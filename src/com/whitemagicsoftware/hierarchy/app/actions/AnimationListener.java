/* Copyright 2009 White Magic Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.whitemagicsoftware.hierarchy.app.actions;

import prefuse.Display;

/**
 * Used by classes that must be notified when a Display instance has finished
 * animating its graph.
 */
public interface AnimationListener {
  /**
   * Implemented by classes to receive a notification of when the given
   * Display instance has finished animating its graph.
   * 
   * @param display The display that was animating a graph.
   */
  public void animationFinished( Display display );
}
