/* Copyright 2009 White Magic Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.whitemagicsoftware.hierarchy.app;


import com.whitemagicsoftware.util.MultiValueMap;

import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Line2D;
import java.awt.geom.Path2D;
import java.awt.geom.Rectangle2D;

import java.util.HashSet;
import java.util.Set;

import prefuse.Display;

import prefuse.data.Node;

import prefuse.render.EdgeRenderer;

import prefuse.util.display.PaintListener;

import prefuse.visual.EdgeItem;
import prefuse.visual.VisualItem;


/**
 * Responsible for drawing orthogonal edges between two nodes, with an
 * arrow head.
 */
public class OrthogonalEdgeRenderer
  extends EdgeRenderer
  implements PaintListener {
  /** Limit for the arrow width. */
  private static final int MAX_ARROW_WIDTH = 32;

  /** Limit for the arrow height. */
  private static final int MAX_ARROW_HEIGHT = 32;

  /** Visually pleasing size for the arrow head width. */
  private static final int DEFAULT_ARROW_WIDTH = 9;

  /** Visually pleasing size for the arrow head height. */
  private static final int DEFAULT_ARROW_HEIGHT = 9;

  /** Shape reused for drawing arrows and lines. */
  private Path2D.Double shape = new Path2D.Double();

  /** List of nodes that have already been examined. */
  private Set<Node> previousNodeSources = new HashSet<Node>( 1024 );

  /** Mid-points to line segments that have already been drawn. */
  private MultiValueMap<Integer, Line2D.Double> midPointMap =
    new MultiValueMap<Integer, Line2D.Double>();

  /**
   * Creates a new edge renderer with a UML-like arrow pointing to the parent.
   * The default arrow size is 15x15 pixels.
   */
  public OrthogonalEdgeRenderer() {
    setArrowHeadSize( DEFAULT_ARROW_WIDTH, DEFAULT_ARROW_HEIGHT );
  }

  private MultiValueMap<Integer, Line2D.Double> getPointMap() {
    return this.midPointMap;
  }

  private Set<Node> getPreviousNodeSources() {
    return this.previousNodeSources;
  }
  
  /**
   * Changes the width and height of the arrow, restricting its minimum and
   * maximum extents between 1 and MAX_ARROW_WIDTH or 1 and MAX_ARROW_HEIGHT.
   * 
   * @param w Width for the arrow head.
   * @param h Height for the arrow head.
   */
  public void setArrowHeadSize( int w, int h ) {
    if( w <= MAX_ARROW_WIDTH && h <= MAX_ARROW_HEIGHT && w > 0 && h > 0 ) {
      super.setArrowHeadSize( w, h );
    }
  }

  /**
   * Creates an orthogonal shape (an edge) to draw between two nodes.
   *
   * @param vi The visual item with start and end points.
   * @return The shape to draw between the nodes for this edge.
   */
  protected Shape getRawShape( VisualItem vi ) {
    Path2D.Double result = getShape();

    // If line segments would overlap, this gets set to false.
    //
    boolean drawSegment = true;

    if( vi instanceof EdgeItem ) {
      EdgeItem ei = ( EdgeItem )vi;
      Rectangle2D source = ei.getSourceItem().getBounds();

      double sx = ei.getSourceItem().getX();
      double sy = source.getY() + source.getHeight();
      double tx = ei.getTargetItem().getX();
      double ty = ei.getTargetItem().getBounds().getY();

      int midy = ( int )( sy + ty ) >> 1;

      int aw = getArrowHeadWidth() >> 1;
      int ah = getArrowHeadHeight();

      result.reset();

      // Lines for the arrow head and line to the bus, if not already drawn.
      //
      if( getPreviousNodeSources().add( ei.getSourceNode() ) ) {
        // Lines for the arrow head.
        //
        result.moveTo( sx, sy );
        result.lineTo( sx - aw, sy + ah );
        result.lineTo( sx + aw, sy + ah );
        result.lineTo( sx, sy );

        // Line from the arrow head to the bus (think computer bus).
        //
        if( sx != tx ) {
          result.moveTo( sx, sy + ah );
          result.lineTo( sx, midy );
        }
      }

      Line2D.Double segment = new Line2D.Double( sx, midy, tx, midy );

      // Associate the middle-y point with the bounds of the target object.
      // On subsequent draws of targets with a similar mid-y, make sure that
      // there are no overlapping lines.
      //
      if( getPointMap().put( midy, segment ) != null ) {
        // Check previous lines for overlap. Each previous line segment has
        // values in the form: (sx, mid-y)-(tx, mid-y), which map to
        // (getX1(), midy)-(getX2(), midy).
        //
        for( Line2D.Double psegment : getPointMap().getValues( midy ) ) {
          double psx = psegment.getX1();
          double ptx = psegment.getX2();

          // If the lines have the same source point, and differ in their
          // target point, then they might overlap
          //
          if( sx == psx && tx != ptx ) {
            // Previous and current differences between source/target x values.
            //
            double pdx = psx - ptx;
            double cdx = sx - tx;

            // At this juncture: the mid-y points are the same, the source
            // points of the previous segment and the current segment are the
            // same, and the target points of the segments differ.
            //
            // If both lines go in the same direction (relative to the same
            // source point), then they overlap. The difference of the tx
            // and X2 points is how much overlap exists.
            //
            // There are two actionable possibilities: (1) psegment is longer
            // than the current segment; or (2) psegment is shorter.
            //
            // If psegment is longer, then no segment must be drawn. If
            // psegment is shorter, the difference between psegment and the
            // current segment must be drawn.
            //
            if( tx < sx && ptx < sx ) {
              // SEGMENT IS TO THE LEFT OF SOURCE
              //
              if( pdx > cdx ) {
                // If the previous segment is longer, then draw nothing.
                //
                drawSegment = false;
              }
              else {
                // If the previous segment is shorter, then draw the
                // difference. That is, change the source point for
                // this segment to the target point of the previous segment.
                //
                sx = ptx;
              }
            }
            else if( tx > sx && ptx > sx ) {
              // SEGMENT IS TO THE RIGHT OF SOURCE
              //
              if( pdx < cdx ) {
                // If the previous segment is longer, then draw nothing.
                //
                drawSegment = false;
              }
              else {
                // If the previous segment is shorter, then draw the
                // difference. That is, change the source point for
                // this segment to the target point of the previous segment.
                //
                sx = ptx;
              }
            }
          }
        }
      }

      // Line from the bus to the target node.
      //
      if( sx != tx ) {
        // Draw the line for the bus.
        //
        if( drawSegment ) {
          result.moveTo( sx, midy );
          result.lineTo( tx, midy );
        }

        result.moveTo( tx, midy );
        result.lineTo( tx, ty );
      }
      else {
        // This avoids "knuckles" when the source and targets are vertically
        // aligned. The knuckles could also be avoided by adding and subtracting
        // 1, however that value of 1 would have to be scaled.
        //
        result.moveTo( sx, sy + ah );
        result.lineTo( tx, ty );
      }
    }

    return result;
  }

  /**
   * Returns the shape used for drawing arrows and lines. This object is
   * reset and reused per visual item drawn, rather than instantiated each
   * time. This is typically the lines required to connect to nodes (the
   * edge between them).
   *
   * @return The path (including arrow head) that should be drawn.
   *
   * @see #getRawShape( VisualItem )
   */
  private Path2D.Double getShape() {
    return this.shape;
  }

  /**
   * Called before painting has started to clear out memory of the arrows and
   * lines that have already been drawn.
   *
   * @param d Ignored.
   * @param g Ignored.
   */
  public void prePaint( Display d, Graphics2D g ) {
    getPreviousNodeSources().clear();
    getPointMap().clear();
  }

  /**
   * Unused.
   *
   * @param d Ignored.
   * @param g Ignored.
   */
  public void postPaint( Display d, Graphics2D g ) {
  }
}
