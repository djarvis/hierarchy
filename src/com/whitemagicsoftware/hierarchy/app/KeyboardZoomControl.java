package com.whitemagicsoftware.hierarchy.app;

import java.awt.Point;
import java.awt.event.KeyEvent;

import prefuse.Display;

import prefuse.controls.AbstractZoomControl;

import prefuse.visual.VisualItem;


 /**
  * <p>
  * Responsible for providing keyboard shortcuts to zoom the display.
  * </p>
  * <p>
  * <a href="http://goosebumps4all.net/34all/bb/showthread.php?tid=83">Original
  * source</a>.
  * </p>
  *
  * @author Dave Jarvis
  */
public class KeyboardZoomControl
  extends AbstractZoomControl {
  private Point p = new Point();
  private static final char ZOOM_IN = '+';
  private static final char ZOOM_OUT = '-';

  /**
   * Called when a key is pressed. This method will increase or descrease the
   * zoom's display depending on which character was pressed.
   * 
   * @param e Contains the character that fired the key pressed event.
   */
  public void keyPressed( KeyEvent e ) {
    Display display = ( Display )e.getComponent();
    p.x = display.getWidth() / 2;
    p.y = display.getHeight() / 2;

    if( e.getKeyChar() == ZOOM_IN ) {
      zoom( display, p, 1 + 0.1f, false );
    }
    else if( e.getKeyChar() == ZOOM_OUT ) {
      zoom( display, p, 1 - 0.1f, false );
    }
  }

  /**
   * @param item Ignored
   * @param e Passed into keyPressed.
   * @see #keyPressed(KeyEvent)
   */
  public void itemKeyPressed( VisualItem item, KeyEvent e ) {
    keyPressed( e );
  }
}
