/* Copyright 2009 White Magic Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.whitemagicsoftware.hierarchy.app;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.KeyStroke;

import prefuse.Constants;
import prefuse.Display;
import prefuse.Visualization;

import prefuse.action.ActionList;
import prefuse.action.RepaintAction;
import prefuse.action.animate.LocationAnimator;
import prefuse.action.layout.CollapsedSubtreeLayout;
import prefuse.action.layout.graph.NodeLinkTreeLayout;

import prefuse.activity.SlowInSlowOutPacer;

import prefuse.render.EdgeRenderer;
import prefuse.render.LabelRenderer;

/**
 * Not used. This class adds the ability to re-orient the view in one of
 * four ways. This class can be used when the superclass can properly
 * draw orthogonal edges (and arrow heads) between nodes.
 */
public class OrientedTreeView
  extends TreeView {
  private static final long serialVersionUID = 1988941592126611209L;

  /** ID for the graph's orientation actions. */
  private static final String ORIENT = "orient";

  public void initInterface() {
    super.initInterface();
    
    registerKeyboardAction( new OrientAction( Constants.ORIENT_LEFT_RIGHT ),
                            "left-to-right",
                            KeyStroke.getKeyStroke( "ctrl 1" ),
                            Display.WHEN_FOCUSED );
    registerKeyboardAction( new OrientAction( Constants.ORIENT_TOP_BOTTOM ),
                            "top-to-bottom",
                            KeyStroke.getKeyStroke( "ctrl 2" ),
                            Display.WHEN_FOCUSED );
    registerKeyboardAction( new OrientAction( Constants.ORIENT_RIGHT_LEFT ),
                            "right-to-left",
                            KeyStroke.getKeyStroke( "ctrl 3" ),
                            Display.WHEN_FOCUSED );
    registerKeyboardAction( new OrientAction( Constants.ORIENT_BOTTOM_TOP ),
                            "bottom-to-top",
                            KeyStroke.getKeyStroke( "ctrl 4" ),
                            Display.WHEN_FOCUSED );
  }

  /**
   * Attaches orientation action filters to the visualization.
   */
  public void display() {
    super.display();
    getVisualization().putAction( ORIENT, createOrientFilters() );
  }

  /**
   * Creates a list of various filters responsible for orienting the graph.
   * These orientation filters Location, followed by a Repaint action.
   *
   * @return A list of actions to perform during graph re-orientation, never
   * null.
   */
  private ActionList createOrientFilters() {
    ActionList orient = new ActionList( ANIMATION_SPEED );
    orient.setPacingFunction( new SlowInSlowOutPacer() );
    orient.add( new LocationAnimator( NODES ) );
    orient.add( new RepaintAction() );

    return orient;
  }

  /**
   * Changes the orientation of the graph, upon request. This is called when
   * the user presses one of the control-key sequences to alter how the graph
   * is laid out.
   *
   * @param orientation The new orientation for the graph.
   */
  protected void setOrientation( int orientation ) {
    Visualization vis = getVisualization();
    LabelRenderer nodeRenderer = getNodeRenderer();
    EdgeRenderer edgeRenderer = getEdgeRenderer();

    NodeLinkTreeLayout rtl = ( NodeLinkTreeLayout )vis.getAction( LAYOUT );
    CollapsedSubtreeLayout stl =
      ( CollapsedSubtreeLayout )vis.getAction( SUBLAYOUT );

    switch( orientation ) {
      case Constants.ORIENT_LEFT_RIGHT: {
          nodeRenderer.setHorizontalAlignment( Constants.LEFT );
          edgeRenderer.setHorizontalAlignment1( Constants.RIGHT );
          edgeRenderer.setHorizontalAlignment2( Constants.LEFT );
          edgeRenderer.setVerticalAlignment1( Constants.CENTER );
          edgeRenderer.setVerticalAlignment2( Constants.CENTER );
          break;
        }
      case Constants.ORIENT_RIGHT_LEFT: {
          nodeRenderer.setHorizontalAlignment( Constants.RIGHT );
          edgeRenderer.setHorizontalAlignment1( Constants.LEFT );
          edgeRenderer.setHorizontalAlignment2( Constants.RIGHT );
          edgeRenderer.setVerticalAlignment1( Constants.CENTER );
          edgeRenderer.setVerticalAlignment2( Constants.CENTER );
          break;
        }
      case Constants.ORIENT_TOP_BOTTOM: {
          nodeRenderer.setHorizontalAlignment( Constants.CENTER );
          edgeRenderer.setHorizontalAlignment1( Constants.CENTER );
          edgeRenderer.setHorizontalAlignment2( Constants.CENTER );
          edgeRenderer.setVerticalAlignment1( Constants.BOTTOM );
          edgeRenderer.setVerticalAlignment2( Constants.TOP );
          break;
        }
      case Constants.ORIENT_BOTTOM_TOP: {
          nodeRenderer.setHorizontalAlignment( Constants.CENTER );
          edgeRenderer.setHorizontalAlignment1( Constants.CENTER );
          edgeRenderer.setHorizontalAlignment2( Constants.CENTER );
          edgeRenderer.setVerticalAlignment1( Constants.TOP );
          edgeRenderer.setVerticalAlignment2( Constants.BOTTOM );
          break;
        }
    }

    rtl.setOrientation( orientation );
    stl.setOrientation( orientation );

    super.setOrientation( orientation );
  }

  /**
   * Responsible for changing the graph's orientation.
   */
  public class OrientAction
    extends AbstractAction {
    private static final long serialVersionUID = 2533300620911123415L;

    private int orient;

    /**
     * Creates a new OreintAction that is mapped to keystrokes.
     *
     * @param orientation The orientation to apply on a given keystroke.
     */
    public OrientAction( int orientation ) {
      this.orient = orientation;
    }

    /**
     * Changes the orientation upon user request.
     *
     * @param event The event that triggered the action (usually a keypress).
     */
    public void actionPerformed( ActionEvent event ) {
      setOrientation( this.orient );
      getVisualization().cancel( ORIENT );
      getVisualization().run( LAYOUT );
      getVisualization().run( ORIENT );
    }
  }
}
