/* Copyright 2009 White Magic Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.whitemagicsoftware.hierarchy.app;


import com.whitemagicsoftware.hierarchy.app.actions.AnimationListener;

import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.geom.Rectangle2D;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.KeyStroke;

import org.apache.bcel.classfile.JavaClass;

import prefuse.Constants;
import prefuse.Display;
import prefuse.Visualization;

import prefuse.action.Action;
import prefuse.action.ActionList;
import prefuse.action.GroupAction;
import prefuse.action.RepaintAction;
import prefuse.action.animate.ColorAnimator;
import prefuse.action.animate.LocationAnimator;
import prefuse.action.animate.VisibilityAnimator;
import prefuse.action.assignment.ColorAction;
import prefuse.action.assignment.FontAction;
import prefuse.action.layout.CollapsedSubtreeLayout;
import prefuse.action.layout.Layout;
import prefuse.action.layout.graph.NodeLinkTreeLayout;

import prefuse.activity.SlowInSlowOutPacer;

import prefuse.controls.DragControl;
import prefuse.controls.FocusControl;
import prefuse.controls.PanControl;
import prefuse.controls.ZoomControl;
import prefuse.controls.ZoomToFitControl;

import prefuse.data.Graph;
import prefuse.data.Node;

import prefuse.render.DefaultRendererFactory;
import prefuse.render.LabelRenderer;
import prefuse.render.RendererFactory;
import prefuse.render.ShapeRenderer;

import prefuse.util.ColorLib;
import prefuse.util.FontLib;
import prefuse.util.GraphicsLib;
import prefuse.util.display.DisplayLib;
import prefuse.util.display.ExportDisplayAction;

import prefuse.visual.VisualItem;
import prefuse.visual.expression.InGroupPredicate;
import prefuse.visual.sort.TreeDepthItemSorter;


/**
 * <p>
 * Responsible for laying out the graph's visual representation. This class
 * sets up the various parts of a prefuse graph, including: fonts, colours,
 * animations, layout, and renderers.
 * </p>
 *
 * @author Dave Jarvis
 */
public class TreeView
  extends Display
  implements ComponentListener {
  private static final long serialVersionUID = 1422299519800234304L;

  /** Used to indicate the graph's nodes can point to descendents. */
  private static final boolean GRAPH_TYPE_DIRECTED = true;

  /** ID for the graph. */
  protected static final String GRAPH = "graph";

  /** ID for the graph's nodes. */
  protected static final String NODES = GRAPH + ".nodes";

  /** ID for the graph's edges. */
  protected static final String EDGES = GRAPH + ".edges";

  /** ID for the graph's main layout engine. */
  protected static final String LAYOUT = "layout";

  /** ID for the graph's sublayout engine. */
  protected static final String SUBLAYOUT = "sublayout";

  /** ID for the graph's filter actions. */
  protected static final String FILTER = "filter";

  /** ID for the animation actions. */
  protected static final String ANIMATE = "animate";

  /** ID for zoom to fit action. */
  protected static final String ZOOM = "zoom";

  protected static final String NODE_NAME = "label";
  protected static final String NODE_ABSTRACT = "abstract";
  protected static final String NODE_ENUM = "enum";
  protected static final String NODE_FINAL = "final";
  protected static final String NODE_PRIVATE = "private";
  protected static final String NODE_PROTECTED = "protected";
  protected static final String NODE_PUBLIC = "public";

  /** Animation delay (in milliseconds). */
  protected static final int ANIMATION_SPEED = 500;

  /** Milliseconds to wait until resize has finished for zooming. */
  protected static final int AUTO_ZOOM_DELAY = 100;

  /** ID for the quit keystroke. */
  private static final String ACTION_QUIT = "action.quit";

  /** ID for the save keystroke. */
  private static final String ACTION_SAVE = "action.save";

  /** Model for the hierarchy. */
  private Graph graph;

  /** Used for colouring labels on the visual graph. */
  private LabelRenderer shapeRenderer;

  /** Used for colouring edges and arrows on the visual graph. */
  private OrthogonalEdgeRenderer edgeRenderer;

  /** Used when creating nodes. */
  private RendererFactory rendererFactory;

  /** How the main graph is laid out. */
  private int orientation = Constants.ORIENT_TOP_BOTTOM;

  /** Timer for the zoom to fit action (triggered on component resizing). */
  private Timer zoomTimer = new Timer();

  /** Used so that other components know when it is safe to get the graph. */
  private Timer animateTimer = new Timer();

  /** Used to trigger notifications that the display has finished animating. */
  private AnimateAfterAction animateAfterAction = new AnimateAfterAction();

  private List<AnimationListener> animationListeners =
    new ArrayList<AnimationListener>();

  /** Set to true when animation (zooming) has started. */
  private boolean prepareZoom = false;

  /** Font name used when rendering labels. */
  private String fontName = "Verdana";

  /** Font size used when rendering labels. */
  private int fontSize = 16;

  /**
   * Creates a high-quality view of this component.
   */
  public TreeView() {
    this( true );
  }

  /**
   * Used to create a JComponent class that can be displayed on a canvas. This
   * will instantiate a visualization that is used to control various aspects
   * of the graph's view.
   *
   * @param highQuality true - Use anti-aliasing for rendering graphics.
   */
  public TreeView( boolean highQuality ) {
    super( new Visualization() );
    setHighQuality( highQuality );
    addPaintListener( getEdgeRenderer() );
  }

  /**
   * Initializes and displays the graph.
   */
  public void display() {
    initInterface();

    Visualization visualization = getVisualization();

    visualization.add( GRAPH, getGraph() );

    visualization.setRendererFactory( getRendererFactory() );
    visualization.putAction( FILTER, createGraphFilters() );
    visualization.putAction( LAYOUT, createGraphLayout() );
    visualization.putAction( SUBLAYOUT, createGraphSubLayout() );

    visualization.putAction( ZOOM, createZoomToFitAction() );
    visualization.putAction( ANIMATE, createAnimationFilters() );
    visualization.putAction( ANIMATE_AFTER, getAnimateAfterAction() );

    visualization.alwaysRunAfter( FILTER, ANIMATE );
    visualization.alwaysRunAfter( ZOOM, ANIMATE_AFTER );

    // Not allowed to rearrange the nodes (or edges).
    //
    visualization.setInteractive( NODES, null, false );

    visualization.run( FILTER );
    visualization.run( LAYOUT );

    addComponentListener( this );
  }

  private static final String ANIMATE_AFTER = "animate.after";

  /**
   * Returns the action to run after zooming has finished.
   *
   * @return An AnimateAfterAction instance, never null.
   */
  private Action getAnimateAfterAction() {
    return this.animateAfterAction;
  }

  /**
   * Indicates the status of zooming. This is used to help determine when it
   * is safe for other objects to use the graphics context's buffer.
   *
   * @param prepareZoom Set to true when an animation (zoom) has started.
   */
  private void setPrepareZoom( boolean prepareZoom ) {
    this.prepareZoom = prepareZoom;
  }

  /**
   * This method returns true when an animation was requested. If this method
   * returns false and there is no transformation in progress, then it is
   * safe to use the graphics context.
   *
   * @return true - It is not safe to use the graphics context.
   */
  private boolean isPrepareZoom() {
    return prepareZoom;
  }

  /**
   * Returns the list of objects waiting to receive an event when the animation
   * has completely finished.
   *
   * @return The list of listeners to be informed once animation is complete.
   */
  private List<AnimationListener> getAnimationListeners() {
    return animationListeners;
  }

  /**
   * Registers an animation listener that will be notified when animation
   * is complete.
   *
   * @param listener The animation listener to receive animationFinished
   * events.
   */
  public void addAnimationListener( AnimationListener listener ) {
    getAnimationListeners().add( listener );
  }

  /**
   * Unregisters an animation listener from being notified when animation
   * is complete.
   *
   * @param listener The animation listener to no longer receive
   * animationFinished events.
   */
  public void removeAnimationListener( AnimationListener listener ) {
    getAnimationListeners().remove( listener );
  }

  /**
   * Called when animation occurs. This class is responsible for informing
   * observers that animation has completed. One possible use is for saving
   * the display to a file. Before the export can work, the exporter needs
   * to be told that the animations have finished.
   */
  private class AnimateAfterAction
    extends Action {
    /**
     * <p>
     * Called when the animation (zooming) may be complete. In actuality,
     * zooming is considered "finished" during any of its many rendering
     * steps. This means that the prefuse API cannot be used to determine
     * when, exactly, the animation (for zooming) has finished.
     * </p>
     * <p>
     * The only way to be sure that the zooming has finished is to check
     * whether or not a transformation is in progress and a previous zoom
     * has not been requested (i.e., isPrepareZoom == false).
     * </p>
     *
     * @param frac Ignored.
     */
    public void run( double frac ) {
      if( isTranformInProgress() || isPrepareZoom() ) {
        animateTimer.cancel();
        animateTimer = new Timer();

        // The 2 is arbitrary -- there's no need to check the end of the
        // transformation all the time. About half the animation speed should
        // suffice. This will result in a delay of ANIMATION_SPEED / 2
        // milliseconds at most between the animation being finished and the
        // observer being informed that the animation is complete.
        //
        animateTimer.schedule( new AnimateTask(), ANIMATION_SPEED / 2 );
        setPrepareZoom( false );
      }
      else {
        for( AnimationListener listener : getAnimationListeners() ) {
          listener.animationFinished( TreeView.this );
        }
      }
    }

    /**
     * Used by AnimateAfterAction to check if the animation is still in
     * progress.
     */
    private class AnimateTask
      extends TimerTask {
      public void run() {
        getAnimateAfterAction().run( 0 );
      }
    }
  }

  /**
   * Registers save and quit. Does not allow the superclass to register any
   * keyboard short-cuts (such as ctrl E to "export" the graph to disk).
   */
  @Override
  protected void registerDefaultCommands() {
    registerKeyboardAction( getExportDisplayAction(), ACTION_SAVE,
                            KeyStroke.getKeyStroke( "ctrl S" ), WHEN_FOCUSED );
  }

  /**
   * Called by a different class to listen for the quit action. This allows
   * the call to exit the application be sourced from a single point.
   *
   * @param listener The class that will receive the quit event.
   */
  protected void registerQuitListener( ActionListener listener ) {
    registerKeyboardAction( listener, ACTION_QUIT,
                            KeyStroke.getKeyStroke( "ctrl Q" ), WHEN_FOCUSED );
  }

  /**
   * Returns the action used to be notified of the user request to save the
   * graph.
   *
   * @return An instance of ExportDisplayAction, never null.
   */
  private ExportDisplayAction getExportDisplayAction() {
    return new ExportDisplayAction( this );
  }

  /**
   * Zooms the graph so that it fits the current view port.
   */
  protected void zoomToFit() {
    getVisualization().run( ZOOM );
  }

  /**
   * Delegates to rescheduleZoom.
   *
   * @param e Ignored.
   * @see #rescheduleZoom()
   */
  public void componentResized( ComponentEvent e ) {
    rescheduleZoom();
  }

  /**
   * Calls zoomToFit through a timer task that kicks off every AUTO_ZOOM_DELAY
   * milliseconds.
   */
  private void rescheduleZoom() {
    setPrepareZoom( true );

    // Kill the old timer -- no need to zoom the view each time the component
    // is resized.
    //
    zoomTimer.cancel();

    // Start a new timer to count down the time until the user has stopped
    // resizing the view (AUTO_ZOOM_DELAY).
    //
    zoomTimer = new Timer();

    // Try to zoom the graph after the user has stopped resizing.
    //
    zoomTimer.schedule( new ResizeTask(), AUTO_ZOOM_DELAY );
  }

  /**
   * Unused.
   *
   * @param e Ignored.
   */
  public void componentMoved( ComponentEvent e ) {
  }

  /**
   * Unused.
   *
   * @param e Ignored.
   */
  public void componentShown( ComponentEvent e ) {
  }

  /**
   * Unused.
   *
   * @param e Ignored.
   */
  public void componentHidden( ComponentEvent e ) {
  }

  /**
   * Zooms the current view
   */
  private class ZoomToFitAction
    extends GroupAction {
    /**
     * Creates a new zoom action for the given graph group ID.
     *
     * @param id Usually the value of the GRAPH constant ("graph").
     */
    protected ZoomToFitAction( String id ) {
      super( id );
    }

    /**
     * Zooms the graph until it fills the entire view port (and is centered).
     *
     * @param frac Ignored.
     */
    public void run( double frac ) {
      Display display = TreeView.this;

      if( !isTranformInProgress() ) {
        Rectangle2D bounds = getVisualization().getBounds( GRAPH );
        GraphicsLib.expand( bounds, 50 + ( int )( 1 / getScale() ) );
        DisplayLib.fitViewToBounds( display, bounds, ANIMATION_SPEED * 2 );
      }
      else {
        // Play it again, Sam, but a little later. If a previous zoom was
        // in progress, but a new zoom was requested (for example, a resizing
        // started a zooming, but then another resize took place before the
        // first resize had finished), then reschedule another zoom in a few
        // milliseconds, which should give the previous zoom enough time to
        // finish. If not, repeat. This should not deadlock because the
        // transform in progress will eventually finish.
        //
        rescheduleZoom();
      }
    }
  }

  /**
   * Used by the timer to zoom the graph into view.
   */
  private class ResizeTask
    extends TimerTask {
    /**
     * Called when the timer expires.
     */
    public void run() {
      zoomToFit();
    }
  }

  /**
   * Initializes the user interface controls and size.
   */
  public void initInterface() {
    setItemSorter( new TreeDepthItemSorter() );
    addControlListener( new DragControl() );
    addControlListener( new PanControl() );
    addControlListener( new ZoomToFitControl() );
    addControlListener( new ZoomControl() );
    addControlListener( new KeyboardZoomControl() );
    addControlListener( new FocusControl( 1, FILTER ) );
  }

  /**
   * Creates the graph's main layout (a hierarchical view of the graph data).
   *
   * @return An instance of NodeLinkTreeLayout (never null).
   */
  private Layout createGraphLayout() {
    return new NodeLinkTreeLayout( GRAPH, getOrientation(), 50, 50, 30 );
  }

  /**
   * Creates the layout used for sub-trees. This allows sub-trees to be
   * collapsed (or expanded) when clicked.
   *
   * @return An instance of CollapsedSubtreeLayout (never null).
   */
  private Layout createGraphSubLayout() {
    return new CollapsedSubtreeLayout( GRAPH, getOrientation() );
  }

  /**
   * Creates a list of various filters used for the graph's colours.
   *
   * @return A list of actions, never null.
   */
  private ActionList createGraphFilters() {
    // Black node text.
    //
    ColorAction text = new ColorAction( NODES, VisualItem.TEXTCOLOR );
    text.setDefaultColor( ColorLib.rgb( 0, 0, 128 ) );

    // Blue node text outline.
    //
    ColorAction outline = new ColorAction( NODES, VisualItem.STROKECOLOR );
    outline.setDefaultColor( ColorLib.rgb( 102, 127, 178 ) );

    // Colour of edges.
    //
    ColorAction edges = new ColorAction( EDGES, VisualItem.STROKECOLOR );
    edges.setDefaultColor( ColorLib.rgb( 102, 127, 178 ) );

    // Edges must be filled for the arrows to appear.
    //
    ColorAction arrows = new ColorAction( EDGES, VisualItem.FILLCOLOR );
    arrows.setDefaultColor( ColorLib.rgb( 102, 127, 178 ) );

    FontAction font = new FontAction( NODES, getFont() );

    // Create an action list of color assignments.
    //
    ActionList filter = new ActionList();
    filter.add( text );
    filter.add( outline );
    filter.add( edges );
    filter.add( arrows );
    filter.add( font );

    return filter;
  }

  /**
   * Creates a list of various filters responsible for smoothly animating
   * user interface interactions. These animation actions include Visibility,
   * Location, and Color, followed by a Repaint action.
   *
   * @return A list of actions to perform during animation, never null.
   */
  private ActionList createAnimationFilters() {
    ActionList animate = new ActionList( ANIMATION_SPEED );
    animate.setPacingFunction( new SlowInSlowOutPacer() );
    animate.add( new VisibilityAnimator( GRAPH ) );
    animate.add( new LocationAnimator( NODES ) );
    animate.add( new ColorAnimator( NODES ) );
    animate.add( new RepaintAction() );

    return animate;
  }

  /**
   * Creates an action that can zoom the view port such that the graph is
   * centered and completely visisble.
   *
   * @return A ZoomToFitAction instance, never null.
   */
  private ZoomToFitAction createZoomToFitAction() {
    return new ZoomToFitAction( GRAPH );
  }

  /**
   * Returns an instance of Font, defined by getFontName() and getFontSize().
   *
   * @return An instance of Font, never null.
   * @see #getFontName()
   * @see #getFontSize()
   */
  public Font getFont() {
    return FontLib.getFont( getFontName(), getFontSize() );
  }

  /**
   * Checks to see if the given font name is listed in the GraphicsEnvironment.
   * This will return false if the font name does not exist, or the given
   * font name was null.
   *
   * @param fontName Check the availability of this font name.
   * @return true - The font represented by fontName exists.
   */
  private boolean isValidFont( String fontName ) {
    return fontName == null ? false :
           getAllFontNames().contains( fontName.toLowerCase() );
  }

  /**
   * Returns a list of all the font names (in lower case) that are known to
   * the graphics environment. This will not return null.
   *
   * @return A list of lower case font names.
   */
  private List<String> getAllFontNames() {
    String fontNames[] = getFontNames();

    for( int i = fontNames.length - 1; i >= 0; i-- ) {
      fontNames[ i ] = fontNames[ i ].toLowerCase();
    }

    return Arrays.asList( fontNames );
  }

  /**
   * Returns a list of font family names that can be used to set the font
   * for rendering node labels. The case of the font family names is
   * unaltered.
   *
   * @return A list of font family names.
   */
  protected String[] getFontNames() {
    return getGraphicsEnvironment().getAvailableFontFamilyNames();
  }

  /**
   * Returns a handle to the local graphics environment. This is used to get
   * the list of available font names.
   *
   * @return The local graphics environment.
   */
  private GraphicsEnvironment getGraphicsEnvironment() {
    return GraphicsEnvironment.getLocalGraphicsEnvironment();
  }

  /**
   * Returns the default font name used for rendering text labels.
   *
   * @return "Verdana" by default.
   */
  protected String getFontName() {
    return this.fontName;
  }

  /**
   * Changes the default font used for rendering text labels. The name of the
   * font must exist in the GraphicsEnvironment.
   *
   * @param fontName
   */
  protected void setFontName( String fontName ) {
    if( isValidFont( fontName ) ) {
      this.fontName = fontName;
    }
  }

  /**
   * Returns the default font size used for rendering text labels.
   *
   * @return 16 by default.
   */
  protected int getFontSize() {
    return this.fontSize;
  }

  /**
   * Changes the default font size used for rendering text labels. The font
   * size must be greater than zero.
   *
   * @param fontSize The new font size.
   */
  protected void setFontSize( int fontSize ) {
    if( fontSize > 0 ) {
      this.fontSize = fontSize;
    }
  }

  /**
   * Delegates creation of nodes. This allows clients to use the Graph through
   * an extremely simple API.
   *
   * @param jc The JavaClass representing a node to add to the graph.
   * @return The node that was added to the graph.
   */
  public Node addNode( JavaClass jc ) {
    Node node = getGraph().addNode();

    node.setString( NODE_NAME, jc.getClassName() );
    node.setBoolean( NODE_ABSTRACT, jc.isAbstract() );
    node.setBoolean( NODE_ENUM, jc.isEnum() );
    node.setBoolean( NODE_FINAL, jc.isFinal() );
    node.setBoolean( NODE_PRIVATE, jc.isPrivate() );
    node.setBoolean( NODE_PROTECTED, jc.isProtected() );
    node.setBoolean( NODE_PUBLIC, jc.isPublic() );

    return node;
  }

  /**
   * Delegates creation of edges between nodes. This will first create a node
   * with the given name, then create an edge between it and the parent,
   * directionally from the parent to the child.
   *
   * @param jc The JavaClass for the node to add to the graph.
   * @param parent The parent node for the child node, named 'name'.
   * @return The node that was added to the graph, as a child of parent.
   */
  public Node addEdge( JavaClass jc, Node parent ) {
    Node child = addNode( jc );
    getGraph().addEdge( parent, child );
    return child;
  }

  /**
   * Returns the factory used to create the node renderer.
   *
   * @return An instance of RendererFactory, never null.
   */
  private RendererFactory getRendererFactory() {
    if( this.rendererFactory == null ) {
      this.rendererFactory = createRendererFactory();
    }

    return this.rendererFactory;
  }

  /**
   * Creates a directed graph having nodes with a single label, of type
   * String.
   *
   * @return An instance of Graph ready for adding nodes.
   */
  private Graph createGraph() {
    Graph graph = new Graph( GRAPH_TYPE_DIRECTED );

    graph.addColumn( NODE_NAME, String.class );
    graph.addColumn( NODE_ABSTRACT, boolean.class );
    graph.addColumn( NODE_ENUM, boolean.class );
    graph.addColumn( NODE_FINAL, boolean.class );
    graph.addColumn( NODE_PRIVATE, boolean.class );
    graph.addColumn( NODE_PROTECTED, boolean.class );
    graph.addColumn( NODE_PUBLIC, boolean.class );

    return graph;
  }

  /**
   * Returns the Graph that is used for creating nodes and edges. The API
   * for the graph is exposed through delegation. Even though the Graph is
   * considered the model, the NODE_NAME performs double-duty: it tells the
   * Graph what attribute to assign to each String, and it tells the UI how
   * to get the label for each node.
   *
   * @return An instance of Graph, never null.
   */
  protected Graph getGraph() {
    if( this.graph == null ) {
      this.graph = createGraph();
    }

    return this.graph;
  }

  /**
   * Returns the renderer used for creating labels.
   *
   * @return An instance of LabelRenderer, never null.
   */
  protected LabelRenderer getNodeRenderer() {
    if( this.shapeRenderer == null ) {
      this.shapeRenderer = createNodeRenderer();
    }

    return this.shapeRenderer;
  }

  /**
   * Creates the renderer for nodes (a LabelRenderer). This establishes the
   * link between the graph and nodes (through the NODE_NAME attribute). It sets
   * up the label renderer to draw (without filling, which would draw over
   * the arrow), with sufficient padding so that edges and arrows do not
   * overlap the label text.
   *
   * @return An instance of LabelRenderer, never null.
   */
  private LabelRenderer createNodeRenderer() {
    LabelRenderer renderer = new LabelRenderer( NODE_NAME );
    renderer.setRenderType( ShapeRenderer.RENDER_TYPE_DRAW );
    renderer.setHorizontalPadding( 50 );
    renderer.setVerticalPadding( 10 );
    renderer.setRoundedCorner( 10, 10 );

    return renderer;
  }

  /**
   * Creates the factory for rendering nodes (labels). This attaches the
   * edge renderer to the node renderer.
   *
   * @return An instance of DefaultRendererFactory, never null.
   */
  private RendererFactory createRendererFactory() {
    DefaultRendererFactory rf =
      new DefaultRendererFactory( getNodeRenderer() );

    rf.add( new InGroupPredicate( EDGES ), getEdgeRenderer() );

    return rf;
  }

  /**
   * Returns the renderer used to draw edges between nodes.
   *
   * @return An instance of EdgeRenderer, never null.
   */
  protected OrthogonalEdgeRenderer getEdgeRenderer() {
    if( this.edgeRenderer == null ) {
      this.edgeRenderer = createEdgeRenderer();
    }

    return this.edgeRenderer;
  }

  /**
   * Creates a new renderer used to draw edges. This sets the renderer to draw
   * using straight lines and forward-facing directed arrow heads. It also
   * sets the size of the arrow head (to 15 x 15 pixels).
   *
   * @return An instance of EdgeRenderer, never null.
   */
  private OrthogonalEdgeRenderer createEdgeRenderer() {
    return new OrthogonalEdgeRenderer();
  }

  /**
   * Returns how the graph is displayed. This will return a value
   * of Constants.{ORIENT_LEFT_RIGHT, ORIENT_RIGHT_LEFT, ORIENT_TOP_BOTTOM,
   * ORIENT_BOTTOM_TOP}.
   *
   * @return The graph's current orientation.
   */
  protected int getOrientation() {
    return this.orientation;
  }

  /**
   * Changes the size of the arrow head. Delegates the settings to the edge
   * renderer.
   *
   * @param width The new arrow head width.
   * @param height The new arrow head height.
   */
  protected void setArrowHeadSize( int width, int height ) {
    getEdgeRenderer().setArrowHeadSize( width, height );
  }

  /**
   * @param orientation
   */
  protected void setOrientation( int orientation ) {
    this.orientation = orientation;
  }
}
