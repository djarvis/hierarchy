/* Copyright 2009 White Magic Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.whitemagicsoftware.hierarchy.app;


import com.whitemagicsoftware.hierarchy.analyzers.PrefuseClassAnalyzer;
import com.whitemagicsoftware.hierarchy.app.actions.AnimationListener;
import com.whitemagicsoftware.hierarchy.exceptions.ClasspathException;
import com.whitemagicsoftware.hierarchy.exceptions.HierarchyNotFoundException;
import com.whitemagicsoftware.util.Tree;
import com.whitemagicsoftware.util.TreeFactory;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import static java.util.Arrays.asList;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.JFrame;
import javax.swing.UIManager;

import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;

import org.apache.bcel.classfile.JavaClass;

import prefuse.Display;

import prefuse.data.Node;

import prefuse.util.io.IOLib;


/**
 * <p>
 * Creates an visual, interactive graph hierarchy for a Java class.
 * </p>
 * <p>
 * This is the main application used for performing class file analysis,
 * and displaying them within a graph. The application consists of three
 * parts: analysis, transformation, and display. The analysis is performed
 * by the ClassAnalyzer tool (specifically the PrefuseClassAnalyzer subclass).
 * The transformation takes the results from the ClassAnalyzer and converts
 * them into a graph suitable for display by prefuse. The display, then,
 * calls upon prefuse to render the data in an interactive, hierarchical
 * format.
 * </p>
 * @author Dave Jarvis
 */
public class Hierarchy
  extends PrefuseClassAnalyzer
  implements ActionListener, AnimationListener {
  /** User interface. */
  private JFrame frame;

  /** prefuse graph. */
  private TreeView treeView;

  /** Determines whether anti-aliasing is enabled (true = enabled). */
  private boolean highQuality;

  /** Non-null indicates that the user desired to save the graph. */
  private String exportFilename = null;

  /** The scale for exporting the graph (resolution). */
  private double exportScale = 2.0;

  /**
   * Default (empty) constructor.
   */
  public Hierarchy() {
  }

  /**
   * Called by the superclass (ClassAnalyzer) to perform the analysis. Once
   * the analysis is complete (without error), the hierarchy is transformed
   * into a graph. The graph may be immediately exported to a file (if so
   * requested by the user).
   */
  public void doAnalysis() {
    try {
      // If the analysis succeeds, then pass the information into prefuse.
      //
      analyze();

      // Prepare the results of the analysis for display using prefuse.
      //
      transmogrify();

      // Display the information from the analysis within a tree hierarchy.
      //
      display();

      // If the user selected to save the graph, then wait for the graph
      // to finish drawing before exporting it to a file.
      //
      if( exportRequested() ) {
        // Before the graph can be saved, it gets animated. This method is
        // called so that this class is notified when the initial animation
        // (pan and zoom) is complete.
        //
        registerAnimationListener();
      }
    }
    catch( ClassNotFoundException ex ) {
      System.err.println( ex.toString() );
    }
    catch( ClasspathException ex ) {
      System.err.println( ex.toString() );
    }
    catch( HierarchyNotFoundException ex ) {
      System.err.println( ex.toString() );
    }
  }

  /**
   * Walks through the class hierarchy (that was previously analyzed), mapping
   * the parent-child relationships into a top-down, traversable tree. When
   * the tree gets traversed, the resulting nodes are transmogrified into the
   * data structures required by prefuse.
   */
  private void transmogrify() {
    Set<Tree> tree = transform();
    Iterator<Tree> i = tree.iterator();

    if( i.hasNext() ) {
      // Demeter, destroyed.
      //
      i.next().walk();
    }
  }

  /**
   * Display the user interface frame, including the prefuse-based TreeView
   * (i.e., the class hierarchy graph). This also sets the native look
   * and feel for the application.
   */
  private void display() {
    applyNativeLookAndFeel();
    getFrame().setVisible( true );
    getTreeView().display();
    getFrame().pack();
    getTreeView().zoomToFit();
  }

  /**
   * Called by doAnalysis to register a listener to receive an event when the
   * animation is finished. This allows the final view of the graph to be
   * saved.
   */
  private void registerAnimationListener() {
    getTreeView().addAnimationListener( this );
  }

  /**
   * Makes the application appear as though it is a native application. This
   * means that Mac and Windows users will not appear confused when the
   * application displays its window frame.
   */
  private void applyNativeLookAndFeel() {
    try {
      UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() );
    }
    catch( Exception e ) {
      ;
    }
  }

  /**
   * Returns true if the filename to export is set and the image scale is
   * greater than zero.
   *
   * @return true - Export the file.
   */
  private boolean exportRequested() {
    return getExportFilename() != null && getExportScale() > 0;
  }

  /**
   * Writes the image out to a file.
   *
   * @param display The instance of display for which the graphics context
   * is to be saved.
   */
  public void animationFinished( Display display ) {
    File file = new File( getExportFilename() );
    FileOutputStream out = null;
    double scale = getExportScale();

    try {
      String format = IOLib.getExtension( file ).toLowerCase();
      out = new FileOutputStream( file );

      treeView.saveImage( out, format, scale );
    }
    catch( Exception e ) {
      System.err.println( "Could not save: " + e.getMessage() );

      try {
        if( out != null ) {
          out.close();
        }
      }
      catch( Exception ex ) {
        ;
      }
    }

    exit();
  }

  /**
   * Returns the frame for the application's user interface. This will create
   * it if it does not already exist.
   *
   * @return An instance of JFrame, never null.
   */
  private JFrame getFrame() {
    if( this.frame == null ) {
      this.frame = createFrame();
    }

    return this.frame;
  }

  /**
   * Returns the application frame title.
   *
   * @return "Hierarchy".
   */
  protected String getFrameTitle() {
    return "Hierarchy";
  }

  /**
   * Creates a new JFrame for containing the UI components. This creates a
   * frame that exits the application when it is closed. The frame title
   * is set, as well as main content: the tree view.
   *
   * @return The JFrame containing the user interface.
   */
  private JFrame createFrame() {
    JFrame frame = new JFrame( getFrameTitle() );

    frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
    frame.setContentPane( getTreeView() );

    return frame;
  }

  /**
   * Returns the content for the JFrame: the graph containing the hierarchy.
   *
   * @return An instance of TreeView, never null.
   */
  private TreeView getTreeView() {
    if( this.treeView == null ) {
      this.treeView = createTreeView();
    }

    return this.treeView;
  }

  /**
   * Creates a new instance of TreeView. The TreeView will lazily create all
   * the objects it needs to be rendered on the panel.
   *
   * @return An instance of TreeView, never null.
   */
  protected TreeView createTreeView() {
    return new TreeView( isHighQuality() );
  }

  private void setHighQuality( boolean highQuality ) {
    this.highQuality = highQuality;
  }

  private boolean isHighQuality() {
    return highQuality;
  }

  /**
   * Called when the user presses Control-Q to exit the program. This action
   * is assigned to the keystroke within the TreeView class (along with
   * other keystrokes).
   *
   * @param e Ignored.
   */
  public void actionPerformed( ActionEvent e ) {
    exit();
  }

  private void setExportFilename( String exportFilename ) {
    this.exportFilename = exportFilename;
  }

  private String getExportFilename() {
    return exportFilename;
  }

  private void setExportScale( double exportScale ) {
    this.exportScale = exportScale;
  }

  private double getExportScale() {
    return exportScale;
  }

  /**
   * Overrides the XML-producing superclass to create a direct integration point
   * between prefuse and the ClassAnalyzer. The superclass provides a simpler
   * implementation, but requires an intermediate format (XML). This class
   * walks the tree, creating prefuse nodes and edges as it goes.
   *
   * @param <Payload> A JavaClass.
   */
  protected class TreeImpl<Payload>
    extends PrefuseTreeImpl<Payload> {
    /**
     * Used to create a hierarchical tree where every tree is a node with
     * data.
     *
     * @param payload The data for this tree (node).
     */
    public TreeImpl( Payload payload ) {
      super( payload );
    }

    public void walk() {
      // Set up the main branch.
      //
      Node branch = addNode( (JavaClass)this.getPayload() );

      // Dive into the leaves.
      //
      for( Tree leaf : getChildren() ) {
        walk( leaf, branch );
      }
    }

    /**
     * Adds edges between children and parents, directed from the parent
     * to the child. This is a recursive method that transforms ClassAnalyzer
     * tree hierarchy structure to prefuse graph structure.
     *
     * @param tree The ClassAnalyzer tree containing subclasses of the
     * given parent.
     * @param parent The parent class to the tree of subclasses.
     */
    protected void walk( Tree tree, Node parent ) {
      Set<Tree> children = new TreeSet<Tree>( tree.getChildren() );

      // Regardless of whether this is a branch or a leaf, the edge for the
      // currently visited node must be added to the graph.
      //
      parent =
          addEdge( (JavaClass)( (TreeImpl)tree ).getPayload(), parent );

      // If there are leaves for this node (meaning the node is a branch),
      // then iterate over all the leaves, making them the next generation of
      // branches. In other words, the current children will grow up to be
      // parents throughout the loop.
      //
      if( !children.isEmpty() ) {
        for( Tree child : children ) {
          walk( child, parent );
        }
      }
    }

    /**
     * Adds a new node to the graph, associating it as a child of the given
     * parent. The direction for the edge is from parent to child.
     *
     * @param jc JavaClass for the node to add to the graph.
     * @return The node that was added to the graph.
     */
    private Node addNode( JavaClass jc ) {
      return getTreeView().addNode( jc );
    }

    /**
     * Adds a new edge to the graph, connecting a child (created using the
     * name) to the given parent. The direction for the edge is from
     * parent to child.
     *
     * @param jc JavaClass for the node to add to the graph.
     * @param parent Parent of the child node to add.
     * @return The node that was added to the graph.
     */
    private Node addEdge( JavaClass jc, Node parent ) {
      return getTreeView().addEdge( jc, parent );
    }
  }

  /**
   * Responsible for determining the type of Tree to create. This class
   * indicates what type of TreeImpl tree instances are to be used for walking
   * the class hierarchy.
   *
   * @param <Payload> A JavaClass.
   */
  protected class TreeFactoryImpl<Payload>
    extends PrefuseTreeFactoryImpl<Payload> {
    /**
     * Creates an instance of Tree with data for the node.
     *
     * @param payload Data for the node.
     * @return A non-null instance of a Tree subclass.
     */
    public Tree create( Payload payload ) {
      return new TreeImpl<Payload>( payload );
    }
  }

  /**
   * Returns the factory that is used to tell the MultiValueMapTransformer what
   * type of trees to instantiate.
   *
   * @return A non-null TreeFactory subclass.
   */
  protected TreeFactory<JavaClass> getTreeFactory() {
    return new TreeFactoryImpl<JavaClass>();
  }

  /**
   * Adds a low-quality setting for graphing.
   *
   * @param args The command line arguments.
   * @return The OptionSet resulting from parsing the command line options.
   * @throws ClasspathException The classpath could not be updated.
   * @throws IOException The classes could not be found.
   */
  protected OptionSet parseOptions( String[] args )
    throws IOException,
           ClasspathException {
    OptionParser parser = getOptionParser();

    OptionSpec<Integer> canvasWidth =
      parser.acceptsAll( asList( "cw", "canvas-width" ),
                         "Initial canvas width (1024 pixels)." )
      .withRequiredArg().ofType( Integer.class ).describedAs( "1024" );

    OptionSpec<Integer> canvasHeight =
      parser.acceptsAll( asList( "ch", "canvas-height" ),
                         "Initial canvas height (768 pixels)." )
      .withRequiredArg().ofType( Integer.class ).describedAs( "768" );

    OptionSpec<String> exportFilename =
      parser.acceptsAll( asList( "ef", "export-file" ),
                         "Save the image to file then exit." )
      .withRequiredArg().ofType( String.class ).describedAs( "filename.png" );

    OptionSpec<Double> exportScale =
      parser.acceptsAll( asList( "es", "export-scale" ),
                         "Scaling factor for image resolution." )
      .withRequiredArg().ofType( Double.class ).describedAs( "2" );

    OptionSpec<Integer> arrowWidth =
      parser.acceptsAll( asList( "aw", "arrow-width" ),
                         "Initial arrow width (9 pixels)." ).withRequiredArg()
      .ofType( Integer.class ).describedAs( "9" );

    OptionSpec<Integer> arrowHeight =
      parser.acceptsAll( asList( "ah", "arrow-height" ),
                         "Initial arrow height (9 pixels)." ).withRequiredArg()
      .ofType( Integer.class ).describedAs( "9" );

    OptionSpec<String> fontName =
      parser.acceptsAll( asList( "fn", "font-name" ),
                         "Initial font name (Verdana)." ).withRequiredArg()
      .ofType( String.class ).describedAs( "Arial, Verdana" );

    OptionSpec<Integer> fontSize =
      parser.acceptsAll( asList( "fs", "font-size" ),
                         "Initial font size (16 pixels)." ).withRequiredArg()
      .ofType( Integer.class ).describedAs( "16" );

    parser.acceptsAll( asList( "df", "display-fonts" ),
                       "List font names then exit." );

    parser.acceptsAll( asList( "lq", "low-quality" ),
                       "Disable anti-aliasing." );

    OptionSet options = super.parseOptions( args );

    if( options.has( "display-fonts" ) ) {
      displayFontNames();
    }

    if( !options.has( "classes" ) ) {
      showHelp();
    }

    setHighQuality( !options.has( "low-quality" ) );

    int cw =
      options.has( "canvas-width" ) ? options.valueOf( canvasWidth ).intValue() :
      1024;
    int ch =
      options.has( "canvas-height" ) ? options.valueOf( canvasHeight ).intValue() :
      768;
    int aw =
      options.has( "arrow-width" ) ? options.valueOf( arrowWidth ).intValue() :
      9;
    int ah =
      options.has( "arrow-height" ) ? options.valueOf( arrowHeight ).intValue() :
      9;
    String fn =
      options.has( "font-name" ) ? options.valueOf( fontName ) : "Verdana";
    int fs =
      options.has( "font-size" ) ? options.valueOf( fontSize ).intValue() : 16;

    if( options.has( "export-file" ) ) {
      setExportFilename( options.valueOf( exportFilename ) );
    }

    if( options.has( "export-scale" ) ) {
      setExportScale( options.valueOf( exportScale ).doubleValue() );
    }

    TreeView treeView = getTreeView();

    treeView.setSize( cw, ch );
    treeView.setArrowHeadSize( aw, ah );
    treeView.setFontName( fn );
    treeView.setFontSize( fs );

    treeView.registerQuitListener( this );

    return options;
  }

  /**
   * Writes the entire list of available font names to standard output.
   */
  private void displayFontNames() {
    for( String font : getTreeView().getFontNames() ) {
      System.out.println( font );
    }

    exit();
  }

  /**
   * Entry point.
   *
   * @param args Command line parameters.
   * @throws IOException The command line arguments could not be written
   * to standard output.
   * @throws ClasspathException Problem happened updating the classpath.
   * @throws InterruptedException The analysis thread bailed.
   */
  public static void main( String[] args )
    throws IOException,
           ClasspathException,
           InterruptedException {
    Hierarchy hierarchy = new Hierarchy();
    Thread thread = new Thread( hierarchy );

    hierarchy.parseOptions( args );
    thread.run();
    thread.join();
  }
}
