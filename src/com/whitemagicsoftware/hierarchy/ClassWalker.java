/* Copyright 2009 White Magic Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.whitemagicsoftware.hierarchy;

import java.io.File;

import java.util.Collection;
import java.util.HashSet;

/**
 * <p>
 * Responsible for recursively scanning a directory, to a default maximum depth
 * of 32 levels. Subclasses can override getMaximumDepth() to return a different
 * default. This does not use Apache's version because Apache's is too complex,
 * slow, and would otherwise incur a dependency for effectively a single,
 * recursive method.
 * </p>
 * <p>
 * This performs a case-sensitive search of files ending in ".class". It also
 * skips any files or directories that are either hidden or have a period at
 * the start of their name.
 * </p>
 */
public class ClassWalker {
  /** Default limit for recursive depth: 32. */
  private static final int DEFAULT_DEPTH = 32;

  /** Set of classes discovered within a directory hierarchy. */
  private Collection<File> collection = new HashSet<File>( 64 );

  /**
   * Default (empty) constructor.
   */
  public ClassWalker() {
  }

  /**
   * Traverses the directory structure starting at the given path, looking
   * for files that end in .class. Each file that satisifies this criteria
   * is then added to the collection, which is returned when all files have
   * been examined.
   * 
   * @param path Directory containing files and, possibly, other directories.
   * @return A collection consisting of class file File references.
   */
  public Collection<File> walk( File path ) {
    Collection<File> c = getCollection();
    
    c.clear();
    walk( path, 0, c );
    return c;
  }

  /**
   * Returns the internal set of classes that have been found within the
   * directory hierarchy. The individual class files could contain anything
   * at this point, no parsing takes place to determine actual content.
   * 
   * @return A collection consisting of File references to class files.
   */
  public Collection<File> getCollection() {
    return this.collection;
  }

  /**
   * Performs a recursive traversal of the given path, up to a fixed maximum
   * depth.
   * 
   * @param path Directory containing files and, possibly, other directories.
   * @param depth Current depth in the directory tree.
   * @param collection Set of files whose name end in .class.
   */
  private void walk( File path, int depth, Collection<File> collection ) {
    int childDepth = depth + 1;

    if( childDepth < getMaximumDepth() ) {
      File[] files = path.listFiles();

      if( files != null ) {
        for( int i = files.length - 1; i >= 0; i-- ) {
          File child = files[ i ];

          // Ignore hidden files, or files starting with a period. On
          // Windows, some directories might with a "." because their
          // application wants to emulate the hidden file convention of Unix.
          //
          if( child.isHidden() || child.getName().startsWith( "." ) ) {
            continue;
          }

          if( child.isDirectory() ) {
            walk( child, childDepth, collection );
          }
          else if( child.isFile() &&
                   child.getAbsolutePath().endsWith( ".class" ) ) {
            collection.add( child );
          }
        }
      }
    }
  }

  /**
   * Returns the maximum depth to recurse.
   *
   * @return 32.
   */
  protected int getMaximumDepth() {
    return DEFAULT_DEPTH;
  }
}

