package com.whitemagicsoftware.hierarchy.exceptions;

/**
 * <p>
 * Used to indicate that a class has no discernable hierarchy. This is
 * can be caused by filtering too many classes for exclusion.
 * </p>
 */
public class HierarchyNotFoundException
  extends Exception {
  private static final long serialVersionUID = -6485670155844963782L;

  /**
   * Creates a new exception, formed using a given message.
   * 
   * @param message The class whose hierarchy could not be discerned.
   */
  public HierarchyNotFoundException( String message ) {
    super( message );
  }
}
