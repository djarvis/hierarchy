package com.whitemagicsoftware.hierarchy.exceptions;

/**
 * <p>
 * Used to indicate that the classpath could not be changed. Some reasons
 * include:
 * </p>
 * <ol>
 * <li>Calling Classloader's addURL method failed.</li>
 * <li>Java archive (or directory) could not be found.</li>
 * <li>Java archive was not in the correct file format.</li>
 * <li>Error reading a class file.</li>
 * </ol>
 */
public class ClasspathException
  extends Exception {
  /** <pre>serialver -classpath dist/hierarcy.jar com.whitemagicsoftware.hierarchy.exceptions.ClasspathException</pre> */
  private static final long serialVersionUID = -4067085688031261101L;
  
  /**
   * Creates a new excuse for why the classpath could not be changed, given
   * a good reason.
   * 
   * @param cause The exception that indicates why addURL could not be called.
   */
  public ClasspathException( Throwable cause ) {
    super( cause );
  }
}
