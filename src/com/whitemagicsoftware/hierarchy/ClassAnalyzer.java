/* Copyright 2009 White Magic Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.whitemagicsoftware.hierarchy;


import com.whitemagicsoftware.hierarchy.exceptions.ClasspathException;
import com.whitemagicsoftware.hierarchy.exceptions.HierarchyNotFoundException;
import com.whitemagicsoftware.util.MultiValueMap;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.regex.Pattern;

import org.apache.bcel.Repository;
import org.apache.bcel.classfile.ClassParser;
import org.apache.bcel.classfile.JavaClass;


/**
 * <p>
 * Responsible for finding all superclasses and subclasses for a set of
 * Java classes. The search scope is defined by the classpath. A set of
 * directories or Java archive files may be provided through the API.
 * This class will automatically add the root directory of each of the given
 * Java classes to search for subclasses and superclasses.
 * </p>
 * <p>
 * This class changes the java.class.path System property and the classpath for
 * the current thread's ClassLoader.
 * </p>
 * <p>
 * To prevent the ClassAnalyzer from affecting the application's classpath, the
 * ClassAnalyzer must execute in its own thread.
 * </p>
 * <p>
 * By default no packages are excluded from the search.
 * </p>
 * <p>
 * An instance of ClassAnalyzer will call <code>visitSubclass</code> for
 * <em>all</em> subclasses it can find in the class hierarchy (as defined by
 * the classpath).
 * </p>
 * <p>
 * This class contains a cache of File instances. The cache is initialized
 * using file names from the classpath. Each run of analyze will clear and
 * reparse the classpath.
 * </p>
 *
 * @author Dave Jarvis
 */
public abstract class ClassAnalyzer
  implements Runnable {
  /** File name extension to identify class files. */
  public static final String CLASS_EXTENSION = ".class";

  /** Directory name separator characters: / or \ */
  private static final char FS = File.separatorChar;

  /** Package separator character (converted to FS later): . */
  private static final char PS = '.';

  /** Class files whose full hiearchy is sought. */
  private Set<String> classes = new HashSet<String>( 64 );

  /** Used to scan directories for class files. */
  private ClassWalker classWalker = new ClassWalker();

  /** Classes or packages that should not be included in the results. */
  private Pattern excludeRegex = Pattern.compile( "" );

  /** Associates classes with their subclasses. */
  private MultiValueMap<JavaClass, JavaClass> hierarchy =
    new MultiValueMap<JavaClass, JavaClass>();

  /** Associates parsed classes with their filename. */
  private Map<String, JavaClass> parsedClasses =
    new HashMap<String, JavaClass>( 1024 );

  /** Directories and Java archive file references from the classpath. */
  private Set<File> classpathFiles = new HashSet<File>();

  /**
   * Creates a new ClassAnalyzer that does not visit classes in the java.lang
   * hierarchy.
   */
  public ClassAnalyzer() {
  }

  /**
   * Called from a thread to perform the analysis. This method ensures that
   * the classpath System property remains unmodified when the method finishes.
   * While the run method is executing, the System classpath property (of
   * "java.class.path") will be changed.
   */
  public void run() {
    String path = ClassPathUpdater.getSystemClassPath();
    doAnalysis();
    ClassPathUpdater.setSystemClassPath( path );
  }

  /**
   * Subclasses implement this method to call the <code>analyze()</code>
   * method.
   */
  protected abstract void doAnalysis();

  /**
   * Override to be notified when new a parent-child class relationship has
   * been found.
   *
   * @param parent The superclass of child.
   * @param child A subclass of parent.
   */
  public abstract void visitSubclass( JavaClass parent, JavaClass child );

  /**
   * Override to be notified when a new child-parent class relationship has
   * been found. This method will be called for the depth and breadth of the
   * class hierarchy, starting from the classes adding using the addClass
   * methods.
   *
   * @param parent The superclass of child.
   * @param child A subclass of parent.
   */
  public abstract void visitSuperclass( JavaClass parent, JavaClass child );

  /**
   * Adds an array of class file names to the list of classes to be scanned for
   * super and subclasess. This is useful for command-line implementations
   * where String args[] is passed directly to this method.
   *
   * @param filenames The list of filenames to analyze.
   */
  public void addClasses( String[] filenames ) {
    addClasses( Arrays.asList( filenames ) );
  }

  /**
   * Adds a collection of class file names to the list of classes to be
   * scanned for super and subclasess. This is useful for command-line
   * implementations where String args[] is passed directly to this method.
   *
   * @param filenames The list of filenames to analyze.
   */
  public void addClasses( Collection<String> filenames ) {
    getClasses().addAll( filenames );
  }

  /**
   * Helper method to add the list of classes (specified by a set of
   * files) to the list by using the absolute path name for the file.
   *
   * @param files List of files to add into the hierarchy analysis.
   */
  public void addClassFiles( Collection<File> files ) {
    for( File f : files ) {
      addClass( f.toString() );
    }
  }

  /**
   * Adds a class to the list of classes that will reveal their hierarchy
   * to this instance of ClassAnalyzer.
   *
   * @param filename Fully qualified path of a class file to analyze.
   */
  public void addClass( String filename ) {
    getClasses().add( filename );
  }

  /**
   * <p>
   * Helper method that transforms a typical end-user wildcard match for
   * packages and class names into a regular expression. For example:
   * </p>
   * <p>
   * <code>java.lang.*</code> becomes
   * <code>^java\.lang\..*</code><br />
   *
   * <code>java.lang.*|javax.nio.*</code> becomes
   * <code>^java\.lang\..*|^javax\.nio\..*</code>
   * </p>
   * @param exclude A simple pattern to match packages and classes.
   * @see #setExcludeRegex( String )
   */
  public void excludeSimple( String exclude ) {
    setExcludeRegex( "^" +
                     exclude.replaceAll( "\\.", "\\\\." ).replaceAll( "\\*",
                                                                      ".*" ) );
  }

  /**
   * Sets the regular expression to use for excluding packages and classes.
   * Any fully qualified class name that matches the given regular expression
   * will not be visited.
   *
   * @param excludeRegex Regular expression used to reduce the number of
   * visited classes.
   */
  public void setExcludeRegex( String excludeRegex ) {
    if( excludeRegex == null ) {
      excludeRegex = "";
    }

    this.excludeRegex = Pattern.compile( excludeRegex );
  }

  /**
   * Returns the regular expression pattern used to weed out unwanted
   * visitations.
   *
   * @return The pattern instance used for matching class names.
   */
  private Pattern getExcludeRegex() {
    return this.excludeRegex;
  }

  /**
   * Finds all ascendents and descendents of classes added via the addClass*
   * methods. This method must be called by subclass implementations (to
   * perform the dependency analysis).
   *
   * @throws ClassNotFoundException No class was found at the specified path.
   * @throws ClasspathException The classpath could not be updated to include
   * the root package path to a class.
   * @throws HierarchyNotFoundException No subclasses or superclasses could be
   * determined for the given class.
   */
  protected void analyze()
    throws ClassNotFoundException,
           ClasspathException,
           HierarchyNotFoundException {
    Set<JavaClass> classes = new HashSet<JavaClass>();

    for( String className : getClasses() ) {
      JavaClass jc = getJavaClass( className );

      // If the path to a file was specified (and not just the name of a class),
      // then add that class's root directory to the classpath. The ClassWalker
      // will find any subclasses (or superclasses) that exist within the
      // directory hierarchy.
      //
      if( className.endsWith( CLASS_EXTENSION ) ) {
        updateClassPath( getPackageRootPath( jc ) );
      }

      classes.add( jc );
    }

    initClasspathFiles();

    findSuperclasses( classes );
    findSubclasses( classes );

    // The exclusion filter is probably too strict.
    //
    if( getHierarchy().size() == 0 ) {
      throw new HierarchyNotFoundException( getClasses().toString() );
    }
  }

  /**
   * Returns the difference between the fully qualified path to the given
   * Java class (i.e., jc.getFileName()) and the package in which the Java
   * class resides. For example, if JavaClass was "/home/com/a/b/JC.class",
   * then this method will return "/home", as "/home" should be added to the
   * the classpath.
   *
   * @param jc The class that will determine the new path to add to the
   * classpath (for finding classes).
   *
   * @return A File that represents the directory under which the given
   * Java class can be found.
   */
  private File getPackageRootPath( JavaClass jc ) {
    String path = getClassDirectory( jc.getFileName() ).getAbsolutePath();

    int i = path.lastIndexOf( getPackagePath( jc ) );

    // This means that the package noted for the class was not in the
    // directory hierarchy in which the class was found. While source files
    // do not need to be in a directory of their package's namesake, the
    // class files must reside in a correspondingly named directory.
    //
    if( i == -1 ) {
      throw new IllegalArgumentException( "Package directory mismatch." );
    }

    // The classpath must include the directory under which the package
    // hierarchy for the given class can be found. It is within this directory
    // that other directories must be recursively examined for class files.
    //
    return new File( path.substring( 0, i ) );
  }

  /**
   * Returns an instance of ClassParser based on the fully qualified path
   * to a file. If the input stream is null, this will create a class parser
   * based on the filename alone.
   *
   * @param is The binary stream for a class file (usually a Java archive);
   * can be null.
   * @param filename The fully qualified (not relative) path to a class file.
   *
   * @return A ClassParser instance capable of parsing the binary contents of
   * the file at the local location specified by filename.
   * @throws IOException The file contents could not be read.
   */
  private ClassParser createClassParser( InputStream is, String filename )
    throws IOException {
    return is == null ? new ClassParser( filename ) :
           new ClassParser( is, filename );
  }

  /**
   * Helper method to return the value of the package for the given class.
   * If the package does not exist, this will return the empty string.
   *
   * @param jc Java class whose package name is desired.
   *
   * @return A non-null String.
   */
  private String getPackageName( JavaClass jc ) {
    String packageName = jc.getPackageName();
    return packageName == null ? "" : packageName;
  }

  /**
   * Helper method to return the value of the package in which the given
   * JavaClass can be found, but with its dot-separator notation replaced
   * with the system's file separator character.
   *
   * @param jc Java class whose package name is to be converted to a file path.
   *
   * @return A file path representation of the given Java class' package name.
   */
  private String getPackagePath( JavaClass jc ) {
    return getPackageName( jc ).replace( PS, FS );
  }

  /**
   * Appends a new path to the classloader and the classpath.
   *
   * @param path Extra path (directory or archive) to append to the classpath.
   * @throws ClasspathException A problem happened setting the
   * classpath (see its throwable cause for details).
   */
  public void updateClassPath( File path )
    throws ClasspathException {
    // Add the path to the classpath. This will add it to the Class Loader and
    // the System classpath property.
    //
    ClassPathUpdater.add( path );
  }

  /**
   * Helper method for adding multiple Java archives and directories to the
   * classpath.
   *
   * @param files The list of files to append to the classpath.
   * @throws ClasspathException A problem happened setting the
   * classpath (see its throwable cause for details).
   */
  public void updateClassPath( Collection<File> files )
    throws ClasspathException {
    for( File f : files ) {
      updateClassPath( f );
    }
  }

  /**
   * Finds all the superclasses of the given classes.
   *
   * @param children Set of classes for which the parent hierarchy is to be
   * visited.
   * @throws ClassNotFoundException Could not read superclass.
   */
  private void findSuperclasses( Set<JavaClass> children )
    throws ClassNotFoundException {
    for( JavaClass child : children ) {
      findSuperclasses( child );
    }
  }

  /**
   * Finds all the superclasses of the given class.
   *
   * @param child Class for which the parent hierarchy is to be visited.
   * @throws ClassNotFoundException Could not read superclass.
   */
  protected void findSuperclasses( JavaClass child )
    throws ClassNotFoundException {
    JavaClass parent = child.getSuperClass();

    // Continue visiting superclasses until Object or an unvisitable class
    // is found.
    //
    while( parent != null && canVisit( parent ) && canVisit( child ) ) {
      if( register( parent, child ) ) {
        visitSuperclass( parent, child );
      }

      child = parent;
      parent = child.getSuperClass();
    }
  }

  /**
   * This calls <code>findSubclasses()</code> with a set containing only the
   * given parent parameter.
   *
   * @param parent Find all subclasses of this parameter's class that
   * exist in the classpath.
   * @see #findSubclasses( Set )
   */
  protected void findSubclasses( JavaClass parent ) {
    Set<JavaClass> parents = new HashSet<JavaClass>();
    parents.add( parent );
    findSubclasses( parents );
  }

  /**
   * <p>
   * Finds all subclasses of the given classes that exist anywhere in the
   * classpath. The classpath variable ("java.class.path") must have been
   * updated to include the root path for the package that corresponds to
   * the given JavaClass.
   * </p>
   * <p>
   * To search through additional classpaths (such as java.ext.dirs),
   * either concatentate them manually to the System classpath property
   * (java.class.path), or call <code>updateClassPath</code>.
   * </p>
   *
   * @param parents Find all subclasses of the classes in this set that
   * exist in the classpath.
   */
  protected void findSubclasses( Set<JavaClass> parents ) {
    Set<JavaClass> subclasses = new HashSet<JavaClass>( parents.size() );

    for( File file : getClasspathFiles() ) {
      if( file.isDirectory() ) {
        findDirectorySubclasses( parents, file, subclasses );
      }
      else {
        findArchiveSubclasses( parents, file, subclasses );
      }
    }

    if( subclasses.size() > 0 ) {
      findSubclasses( subclasses );
    }
  }

  /**
   * Splits the classpath into a set of File instances, which are then
   * cached for running <code>findSubclasses</code>. This is run each time
   * analyze is executed.
   *
   * @see #findSubclasses( Set )
   */
  private void initClasspathFiles() {
    Set<File> classpathFiles = getClasspathFiles();

    classpathFiles.clear();

    for( String pathElement : ClassPathUpdater.getClassPathElements() ) {
      File file = new File( pathElement );

      if( file.exists() ) {
        classpathFiles.add( file );
      }
    }
  }

  /**
   * Find all subclasses lurking in classpath directories.
   *
   * @param parents The classes whose subclasses might be found in the given
   * path, or the subdirectories of the given path.
   * @param path The directory structure to search for subclasses of parent.
   */
  private void findDirectorySubclasses( Set<JavaClass> parents, File path,
                                        Set<JavaClass> children ) {
    // If the collection already has files present, then don't try to find
    // all the class files again: reuse the existing list of files.
    //
    Collection<File> files = getClassWalker().getCollection();

    if( files.isEmpty() ) {
      files = getClassWalker().walk( path );
    }

    for( File file : files ) {
      try {
        registerSubclasses( parents, getJavaClass( file.getAbsolutePath() ),
                            children );
      }
      catch( ClassNotFoundException ex ) {
        classNotFound( file );
      }
    }
  }

  /**
   * Find all subclasses lurking in classpath JAR files.
   *
   * @param parents The classes whose subclasses might be in the given archive.
   * @param archive The archive to search for subclasses of the parent.
   */
  private void findArchiveSubclasses( Set<JavaClass> parents, File archive,
                                      Set<JavaClass> children ) {
    JarFile jarFile = null;

    try {
      jarFile = createJarFile( archive );
      Enumeration<JarEntry> e = jarFile.entries();

      while( e.hasMoreElements() ) {
        JarEntry entry = e.nextElement();

        try {
          if( isClassEntry( entry ) ) {
            registerSubclasses( parents, extractSubclass( jarFile, entry ),
                                children );
          }
        }
        catch( ClassNotFoundException ex ) {
          classNotFound( jarFile, entry );
        }
      }
    }
    catch( IOException ex ) {
      archiveNotFound( archive );
    }
    finally {
      // Release the resources for the archive.
      //
      if( jarFile != null ) {
        try {
          jarFile.close();
        }
        catch( IOException ex ) {
          archiveNotClosed( ex );
        }
      }
    }
  }

  /**
   * Prevents having to instantiate multiple ClassWalkers. The ClassWalker
   * resets its result set before starting the recursive descent. A subclass
   * can override this method to return a custom class walker
   *
   * @return A valid instance of ClassWalker.
   */
  protected ClassWalker getClassWalker() {
    return this.classWalker;
  }

  /**
   * Changes the type of ClassWalker used to traverse directories.
   *
   * @param classWalker Knows how to pluck .class files out of a directory
   * structure.
   */
  protected void setClassWalker( ClassWalker classWalker ) {
    this.classWalker = classWalker;
  }

  /**
   * Returns true iff the child is a direct subclass of the parent. If the
   * child class or parent class cannot be found, this method will return
   * false. This cannot use instanceOf because that would invoke visits that
   * are out of direct descendent lineage.
   *
   * @param parent The class to verify as the superclass of child.
   * @param child The class to verify as the subclass of parent.
   *
   * @return true - Child is an instance of parent, but not equal to parent.
   * @throws ClassNotFoundException
   */
  private boolean isSubclass( JavaClass parent, JavaClass child )
    throws ClassNotFoundException {
    JavaClass childSuperclass = child.getSuperClass();
    return childSuperclass != null && childSuperclass.equals( parent );
  }

  /**
   * Helper method to create object representations of Java archive files.
   *
   * @param archive The archive to return as an instance of JarFile.
   * @return A valid instance of JarFile.
   * @throws IOException The Java archive could not be found or opened.
   */
  private JarFile createJarFile( File archive )
    throws IOException {
    return new JarFile( archive );
  }

  /**
   * Visits the subclass iff the child is a subclass of one of the parents.
   * This will cause a number of ClassNotFoundExceptions to be thrown if the
   * classpath is not set correctly.
   *
   * @param parents An unverified set of superclasses for the child.
   * @param child An unverified subclass of parent.
   */
  private void registerSubclasses( Set<JavaClass> parents, JavaClass child,
                                   Set<JavaClass> children )
    throws ClassNotFoundException {
    for( JavaClass parent : parents ) {
      if( isSubclass( parent, child ) && canVisit( child ) &&
          canVisit( parent ) && register( parent, child ) ) {
        children.add( child );
        visitSubclass( parent, child );
      }
    }
  }

  /**
   * <p>
   * Returns true if the given JavaClass does not have a fully qualified
   * classname that matches the regular expression used to weed out unwanted
   * subclasses or superclasses.
   * </p>
   * <p>
   * This will return false for inner classes (specifically identified by
   * including a $ in their name). To visit inner classes as well, override
   * <code>isInnerClass( JavaClass )</code> to return <b>false</b>.
   * </p>
   *
   * @param jc The class to test against exclusion.
   * @return false - Visitation rights have been revoked.
   */
  protected boolean canVisit( JavaClass jc ) {
    String className = jc.getClassName();

    // If the class is an inner class, return false.
    //
    // If the JavaClass name (including package name) matches the regex, then
    // it is not allowed to be visited.
    //
    return !isInnerClass( className ) &&
      getExcludeRegex().matcher( className ).matches() == false;
  }

  /**
   * <p>
   * Returns true if the given class is an inner class. At the moment this
   * looks for a $ symbol in the String. Eventually it should use BCEL
   * to match against all classes, seeing if the given class is an inner class
   * of one of the known classes.
   * </p>
   *
   * @param className The class to identify as an inner class (or not).
   * @return true - The class is an inner class of another class.
   */
  protected boolean isInnerClass( String className ) {
    return className.indexOf( '$' ) > 0;
  }

  /**
   * Returns true if the child was never added to the hierarchy for the given
   * parent before.
   *
   * @param parent The parent of the child.
   * @param child The child of the parent.
   * @return false - The child was already registered for the given parent.
   */
  private boolean register( JavaClass parent, JavaClass child ) {
    return getHierarchy().put( parent, child ) != null;
  }

  /**
   * Creates a JavaClass instance from an entry in the Java archive file.
   * If successful, this will return a JavaClass instance that represents
   * the entry read from the archive.
   *
   * @return null - The JarEntry could not be read as a Java class file.
   */
  private JavaClass extractSubclass( JarFile jf, JarEntry je )
    throws ClassNotFoundException {
    try {
      return isClassEntry( je ) ?
             getJavaClass( jf.getInputStream( je ), je.getName() ) : null;
    }
    catch( IOException ex ) {
      throw new ClassNotFoundException( je.getName(), ex );
    }
  }

  /**
   * Ensures the archive entry is a class file.
   *
   * @return true - The JarEntry represents a Java class file.
   */
  private boolean isClassEntry( JarEntry jarEntry ) {
    return jarEntry == null ? false :
           jarEntry.getName().endsWith( CLASS_EXTENSION );
  }

  /**
   * Helper method to get the directory path for the given file name. If the
   * file name is a directory, then just return a file instance for the given
   * filename. Otherwise, return the directory in which the file rests.
   *
   * @param filename The name of a directory (or file) whose directory (if
   * a file) is to be determined.
   * @return The directory containing filename (or filename if filename is
   * a directory itself).
   */
  private File getClassDirectory( String filename ) {
    File file = new File( filename );

    return file.isDirectory() ? file : file.getParentFile();
  }

  /**
   * Instead of throwing an exception, subclasses can deal with missing
   * archives silently. It's not an exceptional case; more of a warning that
   * should not prevent the processing of further items on the classpath
   * from being analyzed.
   *
   * @param archive The path to the Java archive that could not be opened.
   */
  protected void archiveNotFound( File archive ) {
    System.err.println( "Archive not found: " + archive.getAbsolutePath() );
  }

  /**
   * Called when an archive that was successfully opened could not be closed.
   * This should not happen.
   *
   * @param ex The cause of the archive not getting closed.
   */
  protected void archiveNotClosed( IOException ex ) {
    System.err.println( "Archive not closed: " + ex.toString() );
  }

  /**
   * Helper method called when an entry in the given JarFile instance could
   * not be converted to a class file, even though it appeared to represent
   * a Java class. This is not an exceptional condition, as the process can
   * continue.
   *
   * @param jf The Java archive file being plundered.
   * @param je Entry in the Java archive file that looked to be a class.
   */
  protected void classNotFound( JarFile jf, JarEntry je ) {
    classNotFound( jf.getName() + "::" + je.getName() );
  }

  /**
   * Helper method called when file instance otherwise appeared to represent
   * a Java class. This is not an exceptional condition, as the process can
   * continue.
   *
   * @param file File on disk that looked like a class, but was not a class!
   */
  protected void classNotFound( File file ) {
    classNotFound( file.getAbsolutePath() );
  }

  /**
   * Helper method. Called by the other classNotFound methods with a
   * string representation of the class that was not found. Subclasses should
   * likely override this method.
   *
   * @param path Fully qualified path name (e.g., absolute file path, Java
   * archive entry, and so on).
   */
  protected void classNotFound( String path ) {
    System.err.println( "Class not found: " + path );
  }

  /**
   * Returns the list of class files to analyze.
   *
   * @return A set of filenames.
   */
  private Set<String> getClasses() {
    return this.classes;
  }

  /**
   * Returns list of parent classes that have been mapped to a unique set of
   * subclasses. This will never return null, but it could return an empty
   * map.
   *
   * @return The complete class dependency hiearchy.
   */
  private MultiValueMap<JavaClass, JavaClass> getHierarchy() {
    return this.hierarchy;
  }

  /**
   * Removes knowledge of which classes have been visited. This does not
   * clear the cache of mapped filenames to JavaClass instances. This will
   * also clear out the classpath file cache.
   */
  public void reset() {
    getHierarchy().clear();
    getClasspathFiles().clear();
  }

  /**
   * Returns a representation of the class whose contents are provided by
   * the given InputStream adn whose file name (e.g., an entry in a Java
   * archive) is given by the filename parameter. This caches the JavaClass
   * by its associated name (for faster retrieval later).
   *
   * @param is The binary stream for a class file (usually a Java archive);
   * can be null.
   * @param className Class reference: a file (in an archive), or a fully
   * qualified path to a class on the classpath.
   * @return A JavaClass representing the class found in the given InputStream.
   * @throws ClassNotFoundException The class file could not be opened.
   */
  private JavaClass getJavaClass( InputStream is, String className )
    throws ClassNotFoundException {
    JavaClass jc = getParsedClasses().get( className );

    if( jc == null ) {
      try {
        jc = Repository.lookupClass( className );
      }
      catch( ClassNotFoundException ex ) {
        try {
          jc = createClassParser( is, className ).parse();
        }
        catch( Exception e ) {
          throw new ClassNotFoundException( className );
        }
      }

      getParsedClasses().put( className, jc );
    }

    return jc;
  }

  /**
   * Helper method to get a JavaClass given a class name. The class name
   * can be either a file name or a fully qualified class name set in the
   * classpath.
   *
   * @param className The name of a class to analyze.
   * @return A representation of the compiled class.
   * @throws ClassNotFoundException The class file could not be opened.
   * @see #getJavaClass( InputStream, String )
   */
  private JavaClass getJavaClass( String className )
    throws ClassNotFoundException {
    return getJavaClass( null, className );
  }

  /**
   * Returns a cache all the JavaClass instances that have been created.
   *
   * @return Classes that have been parsed into JavaClass instances based on
   * a filename.
   */
  private Map<String, JavaClass> getParsedClasses() {
    return this.parsedClasses;
  }

  /**
   * Returns the set of classpath file instances.
   *
   * @return A set of files whose names were set in the classpath.
   */
  private Set<File> getClasspathFiles() {
    return this.classpathFiles;
  }
}

